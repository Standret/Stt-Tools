#include "Calculator.h"

namespace computation
{
	const int size_buffer = sizeof(int) * 8;

	calculator::calculator()
	{
		function["sqr"] = [](double x) -> double { return x * x; };
		function["sqrt"] = [](double x) -> double { return std::sqrt(x); };
		function["sin"] = [](double x) -> double { return std::sin(x); };
		function["cos"] = [](double x) -> double { return std::cos(x); };
	}

	double calculator::parse(const char *expression)
	{
		this->expression = expression;
		this->current_position = 0;

		unsigned int position = 0;
		return calculate(expression, position);
	}

	double calculator::calculate(const char *expression, unsigned int &next_arg)
	{
		double res = 0;
		while (expression[next_arg]) {
			res += computation(expression, next_arg);
		}
		return res;
	}

	TypeTerm calculator::get_term(const char *expression)
	{
		switch (expression[0]) {
		case '0': case '1': case '2': case '3': case '4':
		case '5': case '6': case '7': case '8': case '9':
		case '.':
			return number;
		case '*': case '/': case '+': case '-':
			return operation;
		}
	}
	double calculator::get_number(const char *expression, unsigned int &next_arg)
	{
		char buffer[size_buffer];
		int i = 0;
		for (; i < size_buffer && is_number(expression[next_arg + i]); ++i) {
			buffer[i] = expression[i + next_arg];
		}
		next_arg += i;
		return atof(buffer);
	}
	std::string calculator::get_funct_name(const char * expression, unsigned int & next_arg)
	{
		char buffer[size_buffer];

		int i = 0;
		for (; i < size_buffer && is_function(expression[next_arg + i]); ++i) {
			buffer[i] = expression[i + next_arg];
		}
		buffer[i] = '\0';
		next_arg += i;
		return std::string(buffer);
	}
	double calculator::computation(const char *expression, unsigned int &next_arg)
	{
		if (is_number(expression[next_arg])) {
			double res = 0;
			unsigned int next_start = next_arg;
			res = get_number(expression, next_start);
			if (is_priority(expression[next_start]))
				return computation_priority(expression, next_arg);
			next_arg = next_start;
			return res;
		}
		else if (is_function(expression[next_arg]))
			return computation_function(expression, next_arg);

		switch (expression[next_arg]) {
		case '+':
			return computation(expression, ++next_arg);
		case '-':
			return computation(expression, ++next_arg) * -1;
		}
	}
	double calculator::computation_priority(const char *expression, unsigned int &next_arg)
	{
		double res = get_number(expression, next_arg);
		while (is_priority(expression[next_arg])) {
			switch (expression[next_arg]) {
			case '*':
				res *= is_number(expression[++next_arg]) ?
					get_number(expression, next_arg) :
					is_function(expression[next_arg]) ?
					computation_function(expression, next_arg) : 0;
				break;
			case '/':
				res /= is_number(expression[++next_arg]) ?
					get_number(expression, next_arg) :
					is_function(expression[next_arg]) ?
					computation_function(expression, next_arg) : 0;
				break;
			}
		}
		return res;
	}
	double calculator::computation_function(const char *expression, unsigned int &next_arg)
	{
		std::string name = get_funct_name(expression, next_arg);
		if (function.find(name) == function.end())
			return 0;

		if (expression[next_arg] != '(')
			return 0;
		auto numb = is_function(expression[++next_arg]) ? function[name](computation_function(expression, next_arg))
			: function[name](get_number(expression, next_arg));
		if (expression[next_arg++] != ')')
			return 0;

		return numb;
	}
	bool calculator::is_number(const char symbol)
	{
		return symbol == '0' || symbol == '1' || symbol == '2' || symbol == '3' || symbol == '4' || symbol == '5' || symbol == '6' ||
			symbol == '7' || symbol == '8' || symbol == '9' || symbol == '.';
	}
	bool calculator::is_priority(const char symbol)
	{
		return symbol == '*' || symbol == '/';
	}
	bool calculator::is_function(const char symbol)
	{
		if ('a' <= std::tolower(symbol) && std::tolower(symbol) <= 'z')
			return true;
		return false;
	}
}