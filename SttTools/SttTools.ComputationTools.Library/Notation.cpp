#include "Notation.h"

namespace notation
{
	char buffer[256];
	const char *get_mantis(const char *number);
	int strlen_order(const char *expression);

	int Hexadecimal::convert_to_decimal(const char symb)
	{
		if ('0' <= symb && symb <= '9')
			return symb - 48;
		else if ('A' <= symb && symb <= 'F')
			return symb - 55;
		else if ('a' <= symb && symb <= 'f')
			return symb - 87;
		else return 0;
	}
	double Hexadecimal::convert_to_decimal(const char *expression)
	{
		const char *mantis = get_mantis(expression);
		int count_category_order = strlen_order(expression), count_category_mantis = mantis == nullptr ? 0 : std::strlen(mantis);
		double res = 0;
		for (int i = 0; i < count_category_order; ++i) {
			res += Hexadecimal::convert_to_decimal(expression[i]) * std::pow(HEXADECIMAL_NOTATION, count_category_order - 1 - i);
		}
		for (int i = 0; i < count_category_mantis; ++i) {
			res += Hexadecimal::convert_to_decimal(mantis[i]) * std::pow(HEXADECIMAL_NOTATION, 0 - 1 - i);
		}
		return res;
	}
	char *Hexadecimal::convert_to_binary(const char *expression)
	{
		auto temp_res = Hexadecimal::convert_to_decimal(expression);
		std::strcpy(buffer, std::to_string(temp_res).c_str());
		return Decimal::convert_to_binary(buffer);
	}
	int Binary::convert_to_decimal(const char symb)
	{
		return symb - 48;
	}
	double Binary::convert_to_decimal(const char *expression)
	{
		const char *mantis = get_mantis(expression);
		int count_category_order = strlen_order(expression), count_category_mantis = mantis == nullptr ? 0 : std::strlen(mantis);
		double res = 0;
		for (int i = 0; i < count_category_order; ++i) {
			res += Binary::convert_to_decimal(expression[i]) * std::pow(BINARY_NOTATION, count_category_order - 1 - i);
		}
		for (int i = 0; i < count_category_mantis; ++i) {
			res += Binary::convert_to_decimal(mantis[i]) * std::pow(BINARY_NOTATION, 0 - 1 - i);
		}
		return res;
	}
	char *Binary::conert_to_hexadecimal(const char *expression)
	{
		auto temp_res = Binary::convert_to_decimal(expression);
		std::strcpy(buffer, std::to_string(temp_res).c_str());
		return Decimal::convert_to_hexadecimal(buffer);
	}
	const char *get_mantis(const char *number)
	{
		if (number != nullptr) {
			while (*number)
				if (*number++ == '.')
					return number;
		}
		return nullptr;
	}
	int strlen_order(const char *expression)
	{
		int i = 0;
		if (expression != nullptr) {
			while (*expression != '.' && *expression++)
				++i;
		}
		return i;
	}

	char Decimal::convert_to_hexadecimal(int number)
	{
		if (0 <= number && number <= 9)
			return 48 + number;
		else if (10 <= number && number <= 16)
			return number + 55;
		return '0';
	}
	char *Decimal::convert_to_hexadecimal(const char *expression)
	{
		int temp[32]{ 0 };
		const char *mantis = get_mantis(expression);
		int count_category_order = strlen_order(expression), count_category_mantis = mantis == nullptr ? 0 : std::strlen(mantis);
		int order = std::atoi(expression);
		double dmantis = mantis == nullptr ? 0 : std::atoi(mantis) / (std::pow(10, count_category_mantis) * 1.0);

		int countNumber = 0;
		for (; order > 15; ++countNumber) {
			int temp_res = order / 16;
			temp[countNumber] = order - temp_res * 16;
			order = temp_res;
		}
		temp[countNumber++] = order;
		int dump_order = countNumber;

		if (mantis != nullptr)
			for (int i = 0; i < MAX_MANTIS; ++i) {
				double temp_res = dmantis * 16;
				temp[countNumber++] = int(temp_res);
				dmantis = temp_res - int(temp_res);
			}

		// write order
		for (int i = 0; i < dump_order; ++i)
			buffer[i] = Decimal::convert_to_hexadecimal(temp[dump_order - i - 1]);
		//write mantis
		buffer[dump_order] = '.';
		for (int i = dump_order + 1; i < countNumber; ++i)
			buffer[i] = Decimal::convert_to_hexadecimal(temp[i - 1]);
		buffer[countNumber] = '\0';
		return buffer;
	}
	char *Decimal::convert_to_binary(const char *expression)
	{
		int temp[32]{ 0 };
		const char *mantis = get_mantis(expression);
		int count_category_order = strlen_order(expression), count_category_mantis = mantis == nullptr ? 0 : std::strlen(mantis);
		int order = std::atoi(expression);
		double dmantis = mantis == nullptr ? 0 : std::atoi(mantis) / (std::pow(10, count_category_mantis) * 1.0);

		int countNumber = 0;
		for (; order > 1; ++countNumber) {
			int temp_res = order / 2;
			temp[countNumber] = order - temp_res * 2;
			order = temp_res;
		}
		temp[countNumber++] = order;
		int dump_order = countNumber;

		if (mantis != nullptr)
			for (int i = 0; i < MAX_MANTIS; ++i) {
				double temp_res = dmantis * 2;
				temp[countNumber++] = int(temp_res);
				dmantis = temp_res - int(temp_res);
			}

		// write order
		for (int i = 0; i < dump_order; ++i)
			buffer[i] = Decimal::convert_to_hexadecimal(temp[dump_order - i - 1]);
		//write mantis
		buffer[dump_order] = '.';
		for (int i = dump_order + 1; i < countNumber; ++i)
			buffer[i] = Decimal::convert_to_hexadecimal(temp[i - 1]);
		buffer[countNumber] = '\0';
		return buffer;
	}

	//char buffer[256];
	char *convert_notation_to10(char *base, int from)
	{
		//char buffer[256];
		double res = 0;
		if (from == HEXADECIMAL_NOTATION)
			res = Hexadecimal::convert_to_decimal(base);
		else if (from == BINARY_NOTATION) {
			res = Binary::convert_to_decimal(base);
		}
		std::strcpy(buffer, std::to_string(res).c_str());
		return buffer;
	}

	char *convert_notation_to2(char *base, int from)
	{
		//char buffer[256];
		char *res;
		if (from == DECIMAL_NOTATION)
			return Decimal::convert_to_binary(base);
		else if (from == HEXADECIMAL_NOTATION)
			return Hexadecimal::convert_to_binary(base);
		return buffer;
	}

	char *convert_notation_to16(char *base, int from)
	{
		//char buffer[256];
		char *res;
		if (from == DECIMAL_NOTATION)
			return Decimal::convert_to_hexadecimal(base);
		else if (from == BINARY_NOTATION)
			return Binary::conert_to_hexadecimal(base);
		return buffer;
	}

	char* cv(char *base, int from, int to)
	{
		switch (to) {
		case 16:
			return convert_notation_to16(base, from);
		case 10:
			return convert_notation_to10(base, from);
		case 2:
			return convert_notation_to2(base, from);
		}
		return nullptr;
	}
}