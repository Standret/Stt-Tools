#pragma once

#include <cmath>
#include <cstring>
#include <string>

#include "BaseAPI.h"

namespace notation
{
	#define BINARY_NOTATION 2
	#define DECIMAL_NOTATION 10
	#define HEXADECIMAL_NOTATION 16	
	#define MAX_MANTIS 5


	class Hexadecimal
	{
	public:
		static int convert_to_decimal(const char symb);
		static double convert_to_decimal(const char *expression);
		static char *convert_to_binary(const char *expression);
	};

	class Binary
	{
	public:
		static int convert_to_decimal(const char symb);
		static double convert_to_decimal(const char *expression);
		static char *conert_to_hexadecimal(const char *expression);
	};

	class Decimal
	{
	public:
		static char convert_to_hexadecimal(int number);
		static char* convert_to_hexadecimal(const char *expression);
		static char* convert_to_binary(const char *expression);
	};

	const char *get_mantis(const char *number);
	int strlen_order(const char *expression);

	char *convert_notation_to10(char *base, int from);
	char *convert_notation_to2(char *base, int from);
	char *convert_notation_to16(char *base, int from);

	char* cv(char *base, int from, int to);
}