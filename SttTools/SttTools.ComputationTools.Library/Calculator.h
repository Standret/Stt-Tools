#pragma once
#include <cstring>
#include <cstdlib>
#include <map>
#include <string>
#include <cctype>
#include <cmath>

namespace computation
{
	enum TypeTerm
	{
		number, operation, function, expression
	};

	typedef double(*funct)(double);

	class calculator
	{
	public:
		calculator();
		double parse(const char *expression);
	private:
		unsigned int current_position;
		const char *expression;
		std::map<std::string, funct> function;

		double calculate(const char *expression, unsigned int &next_arg);
		TypeTerm get_term(const char *expression);
		double get_number(const char *expression, unsigned int &next_arg);
		std::string get_funct_name(const char *expression, unsigned int &next_arg);

		double computation(const char *expression, unsigned int &next_arg);
		double computation_priority(const char *expression, unsigned int &next_arg);
		double computation_function(const char *expression, unsigned int &next_arg);

		bool is_number(const char symbol);
		bool is_priority(const char symbol);
		bool is_function(const char symbol);
	};
}