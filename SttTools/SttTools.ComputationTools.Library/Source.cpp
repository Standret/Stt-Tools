#include "BaseAPI.h"
#include "Notation.h"
#include "Calculator.h"
#include <vector>
#include <string>
#include <ios>
#include "json.hpp"
#include <cstring>

using json = nlohmann::json;

char buf[50];

using namespace std;

struct Test
{
	std::string in;
	vector<Test> out;
	void parse(nlohmann::basic_json<> &l, nlohmann::basic_json<> &p)
	{
		auto ss = p.dump();
		l["in"] = in;
		for (int i = 0; i < out.size(); ++i) {
			auto ss = l["out"].dump();
			l["out"].push_back({ { "in", out[i].in },{ "out" , json::array() } });
			ss = p.dump();
			out[i].parse(l["out"][i], p);
		}
	}
};


extern "C"
{
	//char* __cdecl _get(char *base, int from, int to)
	//{
	//	//return notation::cv(base, from, to);
	//}

	double __cdecl calculate_expression(char *base)
	{
		computation::calculator calc;
		return calc.parse(base);
	}

	const char* __cdecl json_c()
	{
		json jj;

		Test ob;
		ob.in = "Level 1";
		ob.out.push_back(Test{ "Level 2_1" });
		ob.out.push_back(Test{ "Level 2_2" });
		ob.out[0].out.push_back(Test{ "Level 3_1" });
		ob.out[0].out.push_back(Test{ "Level _2" });
		ob.parse(jj, jj);
		auto dump = jj.dump();
		char *temp = new char[strlen(dump.c_str())];
		strcpy(temp, dump.c_str());
		return temp;
	}
}