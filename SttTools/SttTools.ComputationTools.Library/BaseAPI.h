#pragma once

#define DLLExport(type) extern "C" __declspec(dllexport) type __cdecl
#define DLLExportMethod(type) __declspec(dllexport) type __cdecl