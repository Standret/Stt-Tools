#include "stdafx.h"
#include "src\MeshDocument.h"
#include <cstring>

extern "C"
{
	char buffer[256];
	char* __stdcall test(const char *buff)
	{
		std::strcpy(buffer, buff);
		return buffer;
	}

	paradox_document::mesh::MeshDocument *mesh;
	char *__stdcall load(const char *path)
	{
		mesh = new paradox_document::mesh::MeshDocument(boost::filesystem::path(path));
		mesh->Load();
		return mesh->convert_to_json();
	}
	void __stdcall save()
	{
		delete mesh;
	}
}