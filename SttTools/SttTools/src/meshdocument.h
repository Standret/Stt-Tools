#pragma once

#include "MeshResources.h"
#include "json.hpp"
#include "MeshSection.h"
#include "MeshElement.h"

namespace paradox_document
{
	namespace mesh
	{
		namespace fs = boost::filesystem;
		using json = nlohmann::json;
		using std::string;
		using Resources::TypeMeshElement;
		using Resources::TypeMeshSection;

		class MeshDocument
		{
		public:
			MeshDocument(fs::path path);
			~MeshDocument();
			void Load();
			void Save();
			void Save(const string name);
			string GetName();
			string GetParentFolder();
			inline const MeshSection &get_dataStructure() { return data_structure; }
			long GetSize();
			char *convert_to_json(bool convert_string = true, bool convert_int = false, bool convert_float = false);

			int change_idName(const char *new_name, int id);
		private:
			void InitDocument(fs::path path);
			void InitFile(string name);
			void InitFile(fs::path path);
			void Load(MeshSection &parent, int currentLevel = 0);
			bool IsGeneralId();
			void SetGeneralId();
			int GetLevelSection();
			bool change_idName(MeshSection &parent, const char *new_name, int id);
			TypeMeshSection GetTypeSection();
			TypeMeshElement GetTypeElement();
			string GetNameSection();
			string GetNameElement();
			string GetValueString();
			template<class T> T GetSizeArray();
			template<class T> T *GetElement(int count);
			void LoadElement(MeshSection &section);
			MeshSection data_structure;
			fs::path file_path;
			fs::fstream file;
			char *json_data;
			const string general_id = "@@b@";
		};
	}
}
