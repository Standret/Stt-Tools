#pragma once

namespace paradox_document
{
	namespace mesh
	{
		using std::string;

		template<class T>
		class MeshElement
		{
		public:
			MeshElement(const char *name, const T *value, const int count, const int id);
			MeshElement(const string &name, const T *value, const int count, const int id);
			MeshElement(const MeshElement &object);
			~MeshElement();
			int get_id() const;
			string GetName() const;
			const char *GetName();
			void SetName(const string &name);
			void SetName(const char *name);
			const T *GetValue() const;
			void SetValueString(const char *value, const int count);
			void SetValue(const T *value, const int count);
			int GetCount() const;
			bool operator==(const MeshElement &value);
		private:
			MeshElement();
			int id;
			char *name;
			int count;
			T *value;
		};

		template<class T>
		MeshElement<T>::MeshElement(const char *name, const T *value, const int count, const int id) : name(nullptr), value(nullptr), id(id)
		{
			SetName(name);
			if (typeid(T) == typeid(char))
				SetValueString(reinterpret_cast<const char*>(value), count);
			else
				SetValue(value, count);
		}
		template<class T>
		MeshElement<T>::MeshElement(const string &name, const T *value, const int count, const int id) : name(nullptr), value(nullptr), id(id)
		{
			SetName(name);
			if (typeid(T) == typeid(char))
				SetValueString(reinterpret_cast<const char*>(value), count);
			else
				SetValue(value, count);
		}
		template<class T>
		MeshElement<T>::MeshElement(const MeshElement<T> &object) : id(object.id), name(nullptr), value(nullptr)
		{
			SetName(object.GetName());
			if (typeid(T) == typeid(char))
				SetValueString(reinterpret_cast<const char*>(object.value), object.count);
			else
				SetValue(object.value, object.count);
		}
		template<class T>
		MeshElement<T>::~MeshElement()
		{
			delete[] name;
			delete[] value;
		}
		template<class T>
		inline int MeshElement<T>::get_id() const
		{
			return id;
		}
		template<class T>
		string MeshElement<T>::GetName() const
		{
			return string(name);
		}
		template<class T>
		const char *MeshElement<T>::GetName()
		{
			return name;
		}
		template<class T>
		void MeshElement<T>::SetName(const string &name)
		{
			if (this->name)
				delete[] this->name;
			this->name = new char[name.size() + 1];
			strcpy(this->name, name.c_str());
		}
		template<class T>
		void MeshElement<T>::SetName(const char* name)
		{
			if (this->name)
				delete[] this->name;
			this->name = new char[strlen(name) + 1];
			strcpy(this->name, name);
		}
		template<class T>
		const T *MeshElement<T>::GetValue() const
		{
			return value;
		}
		template<class T>
		void MeshElement<T>::SetValueString(const char * value, const int count)
		{
			if (!this)
				delete[] this->value;
			int size = strlen(value) + 1;
			this->value = new T[sizeof(T) * size];
			this->count = count;
			for (int i = 0; i < size; i++)
				this->value[i] = value[i];
		}
		template<class T>
		void MeshElement<T>::SetValue(const T *value, const int count)
		{
			if (!this)
				delete[] this->value;
			this->value = new T[sizeof(T) * count];
			this->count = count;
			for (int i = 0; i < count; i++)
				this->value[i] = value[i];
		}
		template<class T>
		int MeshElement<T>::GetCount() const
		{
			return count;
		}
		template<class T>
		bool MeshElement<T>::operator==(const MeshElement &value)
		{
			if (this->count == value.count && !strcmp(this->name, value.name))
				for (int i = 0; i < count; i++) {
					if (value.value[i] != this->value[i])
						return false;
				}
			return true;
		}
	}
}