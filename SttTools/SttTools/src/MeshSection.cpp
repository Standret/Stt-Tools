#include "MeshSection.h"

namespace paradox_document
{
	namespace mesh
	{
		MeshSection::MeshSection() : name(nullptr) {}
		MeshSection::MeshSection(const char *name, const int id) : name(nullptr), id(id)
		{
			SetName(name);
		}
		MeshSection::MeshSection(const string &name, const int id) : name(nullptr), id(id)
		{
			SetName(name);
		}
		MeshSection::MeshSection(const MeshSection &object) : name(nullptr), id(object.id)
		{
			SetName(object.name);
			sections = object.sections;
			elementsFloat = object.elementsFloat;
			elementsInt = object.elementsInt;
			elementsString = object.elementsString;
		}

		MeshSection::~MeshSection()
		{
			delete[] name;
		}

		char *MeshSection::convert_to_json(bool convert_string, bool convert_int, bool convert_float)
		{
			nlohmann::json json_object;
			convert_to_json(json_object, json_object, convert_string, convert_int, convert_float);
			auto str = json_object.dump();
			char *temp = new char[str.size() + 1];
			strcpy(temp, str.c_str());
			return temp;
		}

		const string &MeshSection::GetName() const
		{
			return string(name);
		}
		const char *MeshSection::GetName()
		{
			return name;
		}
		void MeshSection::SetName(const string &name)
		{
			if (!this && !this->name)
				delete[] this->name;
			this->name = new char[name.size() + 1];
			strcpy(this->name, name.c_str());
		}
		void MeshSection::SetName(const char *name)
		{
			if (!this && !this->name)
				delete[] this->name;
			this->name = new char[strlen(name) + 1];
			strcpy(this->name, name);
		}

		void MeshSection::SetElement(const MeshElement<int> &value)
		{
			if (!value.GetName().empty())
				elementsInt[value.GetName()].push_back(MeshElement<int>(value));
		}
		void MeshSection::SetElement(const MeshElement<float> &value)
		{
			if (!value.GetName().empty())
				elementsFloat[value.GetName()].push_back(MeshElement<float>(value));
		}
		void MeshSection::SetElement(const MeshElement<char> &value)
		{
			if (!value.GetName().empty())
				elementsString[value.GetName()].push_back(MeshElement<char>(value));
		}
		void MeshSection::SetSection(const MeshSection &section)
		{
			if (!section.GetName().empty())
				sections[section.GetName()].push_back(section);
		}
		void MeshSection::SetSection(const string &name)
		{
			sections[name].push_back(MeshSection(name, get_uniqueId()));
		}

		bool MeshSection::ExistsKey(const string &key, TypeMeshElement type)
		{
			switch (type) {
			case Resources::TypeMeshElement::Int:
				if (elementsInt["key"].size() > 0)
					return true;
				break;
			case Resources::TypeMeshElement::Float:
				if (elementsFloat["key"].size() > 0)
					return true;
				break;
			case Resources::TypeMeshElement::String:
				if (elementsString["key"].size() > 0)
					return true;
				break;
			}
			return false;
		}
		bool MeshSection::ExistsSection(const string &name)
		{
			if (sections[name].size() > 0)
				return true;
			return false;
		}
		bool MeshSection::IsCollision_Element(const string &key, TypeMeshElement type)
		{
			switch (type) {
			case Resources::TypeMeshElement::Int:
				if (elementsInt[key].size() > 1)
					return true;
				break;
			case Resources::TypeMeshElement::Float:
				if (elementsFloat[key].size() > 1)
					return true;
				break;
			case Resources::TypeMeshElement::String:
				if (elementsString[key].size() > 1)
					return true;
				break;
			}
			return false;
		}
		bool MeshSection::IsCollision_Section(const string &name)
		{
			if (sections[name].size() > 1)
				return true;
			return false;
		}

		vector<MeshElement<int>> &MeshSection::GetElementsTypeInt(const string &key)
		{
			if (!ExistsKey(key, TypeMeshElement::Int))
				throw exception::TraceException("invalid key: " + key, TRACE_INFO);
			return elementsInt[key];
		}
		vector<MeshElement<float>> &MeshSection::GetElementsTypeFloat(const string &key)
		{
			if (!ExistsKey(key, TypeMeshElement::Float))
				throw exception::TraceException("invalid key: " + key, TRACE_INFO);
			return elementsFloat[key];
		}
		vector<MeshElement<char>> &MeshSection::GetElementsTypeString(const string &key)
		{
			if (!ExistsKey(key, TypeMeshElement::String))
				throw exception::TraceException("invalid key: " + key, TRACE_INFO);
			return elementsString[key];
		}
		vector<MeshSection> &MeshSection::GetSections(const string &key)
		{
			if (!ExistsSection(key))
				throw exception::TraceException("invalid key: " + key, TRACE_INFO);
			return sections[key];
		}
		map<string, vector<MeshElement<int>>> &MeshSection::GetAllElementsTypeInt()
		{
			return elementsInt;
		}
		map<string, vector<MeshElement<float>>> &MeshSection::GetAllElementsTypeFloat()
		{
			return elementsFloat;
		}
		map<string, vector<MeshElement<char>>> &MeshSection::GetAllElementsTypeString()
		{
			return elementsString;
		}
		map<string, vector<MeshSection>> &MeshSection::GetAllSections()
		{
			return sections;
		}

		inline void MeshSection::UpdateElement(const MeshElement<int> &value, MeshElement<int> &object)
		{
			object = value;
		}
		inline void MeshSection::UpdateElement(const MeshElement<float> &value, MeshElement<float> &object)
		{
			object = value;
		}
		inline void MeshSection::UpdateElement(const MeshElement<char> &value, MeshElement<char> &object)
		{
			object = value;
		}
		inline void MeshSection::UpdateSection(const MeshSection &section, MeshSection &object)
		{
			object = section;
		}

		void MeshSection::RemoveElement(const MeshElement<int> &value)
		{
			if (!ExistsKey(value.GetName(), TypeMeshElement::Int))
				throw exception::TraceException("invalid key: " + value.GetName(), TRACE_INFO);
			auto &rem = elementsInt[value.GetName()];
			if (IsCollision_Element(value.GetName(), TypeMeshElement::Int)) {
				for (int i = 0; i < rem.size(); i++)
					if (rem[i] == value)
						rem.erase(rem.begin() + i);
			}
			else
				rem.clear();
		}
		void MeshSection::RemoveElement(const MeshElement<float> &value)
		{
			if (!ExistsKey(value.GetName(), TypeMeshElement::Float))
				throw exception::TraceException("invalid key: " + value.GetName(), TRACE_INFO);
			auto &rem = elementsFloat[value.GetName()];
			if (IsCollision_Element(value.GetName(), TypeMeshElement::Int)) {
				for (int i = 0; i < rem.size(); i++)
					if (rem[i] == value)
						rem.erase(rem.begin() + i);
			}
			else
				rem.clear();
		}
		void MeshSection::RemoveElement(const MeshElement<char> &value)
		{
			if (!ExistsKey(value.GetName(), TypeMeshElement::String))
				throw exception::TraceException("invalid key: " + value.GetName(), TRACE_INFO);
			auto &rem = elementsString[value.GetName()];
			if (IsCollision_Element(value.GetName(), TypeMeshElement::Int)) {
				for (int i = 0; i < rem.size(); i++)
					if (rem[i] == value)
						rem.erase(rem.begin() + i);
			}
			else
				rem.clear();
		}
		void MeshSection::convert_to_json(nlohmann::basic_json<> &base, nlohmann::basic_json<> &par, 
			bool convert_string, bool convert_int, bool convert_float)
		{
			base["name"] = name;
			base["id"] = id;
			base["element_string"] = nlohmann::json::object();
			base["element_int"] = nlohmann::json::object();
			base["element_float"] = nlohmann::json::object();
			auto str = par.dump();
			int index = 0;
			for (const auto &item : elementsString) {
				base["element_string"].push_back({ item.first, nlohmann::json::array() });
				if (convert_string)
					for (const auto &item_value : item.second) {
						base["element_string"][item.first].push_back({ { "value", item_value.GetValue() }, { "id", item_value.get_id() } });
					}

				++index;
			}
			for (const auto &item : elementsInt) {
				base["element_int"].push_back({ item.first, nlohmann::json::array() });
			}
			for (const auto &item : elementsFloat) {
				base["element_float"].push_back({ item.first, nlohmann::json::array() });
			}
			str = par.dump();
			base["sections"] = nlohmann::json::array();
			index = 0;
			for (auto &item : sections) {
				for (auto &item_value : item.second) {
					item_value.convert_to_json(base["sections"][index++], par, convert_string, convert_int, convert_float);
				}
			}
		}

		int get_uniqueId()
		{
			static int index = 1;
			return index++;
		}
	}
}