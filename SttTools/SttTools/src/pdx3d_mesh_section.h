#pragma once
#include "pdx3d_mesh_element.h"
#include "TraceException.h"
#include <map>
#include <list>

namespace pdx_document
{
	namespace graph
	{
		class mesh_section
		{
		public:
			mesh_section() = delete;
			mesh_section(const std::string &name, unsigned id) : name(name), id(id) { }
			void set_element(int *value, int count, std::string &name);
			void set_element(float *value, int count, std::string &name);
			void set_element(std::string *value, int count, std::string &name);
			void set_section(const std::string &value, unsigned id);

			mesh_section &get_section(unsigned id);
		private:
			std::multimap<std::string, mesh_element<int>> elements_int;
			std::multimap<std::string, mesh_element<float>> elements_float;
			std::multimap<std::string, mesh_element<std::string>> elements_char;
			std::list<mesh_section> sections;
			std::string name;
			unsigned id;
		};

		unsigned get_uniqueId();
	}
}