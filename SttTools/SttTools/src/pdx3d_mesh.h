#pragma once

#include "MeshResources.h"
#include "json.hpp"
#include "pdx3d_mesh_section.h"
#include "TraceException.h"

namespace pdx_document
{
	namespace graph
	{
		namespace fs = boost::filesystem;
		using json = nlohmann::json;
		using std::string;
		using Resources::TypeMeshElement;
		using Resources::TypeMeshSection;

		class mesh
		{
		public:
			mesh() = delete;
			mesh(const std::string &name);
			mesh(const char *name);
			mesh(fs::path &path);
			inline std::string get_name_file() { return full_path.filename().string(); }
			bool load();
		private:
			mesh_section data_structure;
			bool is_supported_version();
			fs::path full_path;
			fs::fstream file;
			const string general_id = "@@b@";

			void load(mesh_section &parent, int currentLevel = 0);
			void load_element(mesh_section &section);

			std::string get_name_section();
			std::string get_name_element();
			std::string *get_value_strings(int count);

			TypeMeshSection get_type_section();
			TypeMeshElement get_type_element();
			size_t get_bytes_size();
			size_t get_int_sizes();
			float* get_float_elements(int count);
			int* get_int_elements(int count);
			int get_level_section();
		};
	}
}