#include "pdx3d_mesh_section.h"

namespace pdx_document
{
	namespace graph
	{
		void mesh_section::set_element(int *value, int count, std::string &name)
		{
			elements_int.insert(std::make_pair(name, mesh_element<int>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_element(float *value, int count, std::string &name)
		{
			elements_float.insert(std::make_pair(name, mesh_element<float>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_element(std::string *value, int count, std::string &name)
		{
			elements_char.insert(std::make_pair(name, mesh_element<std::string>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_section(const std::string &value, unsigned id)
		{
			sections.push_back(mesh_section(value, id));
		}

		mesh_section &mesh_section::get_section(unsigned id)
		{
			for (auto &item : sections) {
				if (item.id == id)
					return item;
			}

			throw TRACE_EXCEPTION("ID not exists: " + std::to_string(id));
		}

		unsigned get_uniqueId()
		{
			static unsigned index = 1;
			return index++;
		}
	}
}