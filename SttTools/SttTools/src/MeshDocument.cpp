#include "MeshDocument.h"

namespace paradox_document
{
	namespace mesh
	{
		MeshDocument::MeshDocument(fs::path path) : json_data(nullptr)
		{
			InitDocument(path);
		}
		MeshDocument::~MeshDocument()
		{
			delete[] json_data;
		}

		void MeshDocument::Load()
		{
			if (!IsGeneralId())
				throw exception::TraceException("Invalig mesh title in file: " + GetName(), TRACE_INFO);
			Load(data_structure);
		}
		void MeshDocument::Save()
		{

		}
		void MeshDocument::Save(const string name)
		{
		}
		string MeshDocument::GetName()
		{
			return string();
		}
		string MeshDocument::GetParentFolder()
		{
			return string();
		}
		long MeshDocument::GetSize()
		{
			return 0;
		}
		char *MeshDocument::convert_to_json(bool convert_string, bool convert_int, bool convert_float)
		{
			json_data = data_structure.convert_to_json(convert_string, convert_int, convert_float);
			return json_data;
		}
		void MeshDocument::InitDocument(fs::path path)
		{
			InitFile(path);
			data_structure.SetName("dataStructure");
		}
		void MeshDocument::InitFile(string name)
		{
			throw exception::NotImplementException(TRACE_INFO);
		}
		void MeshDocument::InitFile(fs::path path)
		{
			if (!fs::exists(path))
				throw exception::TraceException("Invalid path", TRACE_INFO);
			if (fs::is_directory(path))
				throw exception::TraceException("path is directory", TRACE_INFO);
			file_path = path;
			file.open(path, std::ios::binary | std::ios::in);
		}

		void MeshDocument::Load(MeshSection &parent, int currentLevel)
		{
			auto type_section = GetTypeSection();
			while (!file.eof()) {
				switch (type_section) {
				case Resources::TypeMeshSection::Element:
					LoadElement(parent);
					break;
				case Resources::TypeMeshSection::Section:
					int level = GetLevelSection();
					if (currentLevel >= level) {
						for (int i = 0; i < level; i++)
							file.unget();
						return;
					}
					else {
						auto name_section = GetNameSection();
						parent.SetSection(name_section);
						Load(parent.GetSections(name_section).back(), currentLevel + 1);
						break;
					}
					break;
				}
				type_section = GetTypeSection();
			}
		}

		bool MeshDocument::IsGeneralId()
		{
			char *temp = new char[general_id.length()];
			file.read(temp, general_id.length());
			if (strncmp(general_id.c_str(), temp, general_id.length())) {
				delete[] temp;
				return false;
			}
			delete[] temp;
			return true;
		}
		void MeshDocument::SetGeneralId()
		{
			throw exception::NotImplementException(TRACE_INFO);
		}
		int MeshDocument::GetLevelSection()
		{
			int count = 0;
			for (char currentSymb = file.get(); currentSymb == '['; count++, currentSymb = file.get());
			file.unget();
			return count;
		}
		bool MeshDocument::change_idName(MeshSection &parent, const char *new_name, int id)
		{
			for (auto &item : data_structure.GetAllElementsTypeString())
				for (auto &item_v : item.second) 
					if (item_v.get_id() == id) {
						item_v.SetName(new_name);
						return true;
					}

			for (auto &item : data_structure.GetAllElementsTypeInt())
				for (auto &item_v : item.second)
					if (item_v.get_id() == id) {
						item_v.SetName(new_name);
						return true;
					}

			for (auto &item : data_structure.GetAllElementsTypeFloat())
				for (auto &item_v : item.second)
					if (item_v.get_id() == id) {
						item_v.SetName(new_name);
						return true;
					}

			for (auto &section : data_structure.GetAllSections())
				for (auto &concreateSection : section.second)
					change_idName(concreateSection, new_name, id);

			return false;
		}
		int MeshDocument::change_idName(const char *new_name, int id)
		{
			if (change_idName(data_structure, new_name, id))
				return 0;
			return -1;
		}
		TypeMeshSection MeshDocument::GetTypeSection()
		{
			char x;
			file.read(&x, 1);
			if (file.eof())
				return TypeMeshSection::None;
			switch (x) {
			case '!':
				return TypeMeshSection::Element;
				break;
			case '[':
				file.unget();
				return TypeMeshSection::Section;
				break;
			default:
				throw exception::TraceException("incorrect parse meshFile: " + GetName(), TRACE_INFO);
			}
		}
		TypeMeshElement MeshDocument::GetTypeElement()
		{
			char x;
			file.read(&x, 1);
			switch (x) {
			case 'i':
				return TypeMeshElement::Int;
				break;
			case 'f':
				return TypeMeshElement::Float;
				break;
			case 's':
				return TypeMeshElement::String;
				break;
			default:
				throw std::runtime_error("incorrect parse meshFile: " + GetName());
			}
		}
		string MeshDocument::GetNameSection()
		{
			char buffer[512];
			int size = 0;
			for (char currentSymb = file.get(); currentSymb != '\0'; size++, currentSymb = file.get()) {
				if (size >= 512)
					throw exception::TraceException("Out of range", TRACE_INFO);
				buffer[size] = currentSymb;
			}
			buffer[size] = '\0';
			return string(buffer);
		}
		string MeshDocument::GetNameElement()
		{
			char buffer[256];
			auto size = GetSizeArray<char>();
			if (size >= 256)
				throw exception::TraceException("Value string >= 256 symbols", TRACE_INFO);
			file.read(buffer, size);
			buffer[size] = '\0';
			return string(buffer);
		}

		string MeshDocument::GetValueString()
		{
			char buffer[512];
			auto size = GetSizeArray<int>();
			if (size >= 512)
				throw exception::TraceException("Value string >= 512 symbols", TRACE_INFO);
			file.read(buffer, size);
			return string(buffer);
		}

		void MeshDocument::LoadElement(MeshSection &section)
		{
			auto name_element = GetNameElement();
			auto type = GetTypeElement();
			int size_array = -1;
			switch (type) {
			case Resources::TypeMeshElement::Int: {
				size_array = GetSizeArray<int>();
				auto arr = GetElement<int>(size_array);
				section.SetElement(MeshElement<int>(name_element, arr, size_array, get_uniqueId()));
				delete[] arr;
				return;
			}
			case Resources::TypeMeshElement::Float: {
				size_array = GetSizeArray<int>();
				auto arr = GetElement<float>(size_array);
				section.SetElement(MeshElement<float>(name_element, arr, size_array, get_uniqueId()));
				delete[] arr;
				return;
			}
			case Resources::TypeMeshElement::String: {
				size_array = GetSizeArray<int>();
				auto arr = GetValueString();
				section.SetElement(MeshElement<char>(name_element, arr.c_str(), size_array, get_uniqueId()));
				return;
			}
			}
		}

		template<class T>
		T MeshDocument::GetSizeArray()
		{
			T object;
			file.read(reinterpret_cast<char	*>(&object), sizeof(T));
			return object;
		}
		template<class T>
		T *MeshDocument::GetElement(int count)
		{
			T *arr = new T[count];
			file.read(reinterpret_cast<char*>(arr), sizeof(T) * count);
			return arr;
		}
	}
}