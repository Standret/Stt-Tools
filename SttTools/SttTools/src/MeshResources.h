#pragma once

namespace Resources
{
	enum class TypeMeshElement
	{
		Int, Float, String
	};
	enum class TypeMeshSection
	{
		Element, Section, None
	};
}
