#pragma once

#include <vector>
#include <map>

#include "MeshElement.h"
#include "MeshElement.cpp"
#include "json.hpp"
#include "MeshResources.h"
#include "TraceException.h"

namespace paradox_document
{
	namespace mesh
	{
		using std::vector;
		using std::map;
		using Resources::TypeMeshElement;

		class MeshSection
		{
		public:
			MeshSection();
			MeshSection(const char *name, const int id);
			MeshSection(const string &name, const int id);
			MeshSection(const MeshSection &object);
			~MeshSection();
			char *convert_to_json(bool convert_string, bool convert_int, bool convert_float);
			const string &GetName() const;
			const char *GetName();
			bool ExistsKey(const string &key, TypeMeshElement type);
			bool ExistsSection(const string &name);
			bool IsCollision_Element(const string &key, TypeMeshElement type);
			bool IsCollision_Section(const string &name);
			void SetName(const string &name);
			void SetName(const char *name);
			void SetElement(const MeshElement<int> &value);
			void SetElement(const MeshElement<float> &value);
			void SetElement(const MeshElement<char> &value);
			void SetSection(const string &name);
			void SetSection(const MeshSection &section);
			vector<MeshElement<int>> &GetElementsTypeInt(const string &key);
			vector<MeshElement<float>> &GetElementsTypeFloat(const string &key);
			vector<MeshElement<char>> &GetElementsTypeString(const string &key);
			vector<MeshSection> &GetSections(const string &key);
			map<string, vector<MeshElement<int>>> &GetAllElementsTypeInt();
			map<string, vector<MeshElement<float>>> &GetAllElementsTypeFloat();
			map<string, vector<MeshElement<char>>> &GetAllElementsTypeString();
			map<string, vector<MeshSection>> &GetAllSections();
			void UpdateElement(const MeshElement<int> &value, MeshElement<int> &object);
			void UpdateElement(const MeshElement<float> &value, MeshElement<float> &object);
			void UpdateElement(const MeshElement<char> &value, MeshElement<char> &object);
			void UpdateSection(const MeshSection &section, MeshSection &object);
			void RemoveElement(const MeshElement<int> &value);
			void RemoveElement(const MeshElement<float> &value);
			void RemoveElement(const MeshElement<char> &value);
		private:
			void convert_to_json(nlohmann::basic_json<> &base, nlohmann::basic_json<> &par, bool convert_string, bool convert_int, bool convert_float);
			char *name;
			int id;
			map<string, vector<MeshElement<int>>> elementsInt;
			map<string, vector<MeshElement<float>>> elementsFloat;
			map<string, vector<MeshElement<char>>> elementsString;
			map<string, vector<MeshSection>> sections;
		};

		int get_uniqueId();
	}
}