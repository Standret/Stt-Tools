#include "pdx3d_mesh.h"

namespace pdx_document
{
	namespace graph
	{
		mesh::mesh(const std::string &name) : mesh(fs::path(name)) {}
		mesh::mesh(const char *name) : mesh(std::string(name)) {}
		mesh::mesh(fs::path &path) : data_structure(mesh_section("data_structure", -1))
		{
			if (!fs::exists(path))
				throw TRACE_EXCEPTION("Invalid path");
			if (fs::is_directory(path))
				throw TRACE_EXCEPTION("path is directory");
			auto extension = path.extension().string();
			if (extension != ".mesh" && extension != ".anim")
				throw TRACE_EXCEPTION("invalid extension");
			full_path = path;
			file.open(path, std::ios::binary | std::ios::in);
		}

		bool mesh::load()
		{
			if (!is_supported_version())
				throw TRACE_EXCEPTION("unsopported version mesh");
			load(data_structure, 0);
		}
		bool mesh::is_supported_version()
		{
			char *temp = new char[general_id.length()];
			file.read(temp, general_id.length());
			if (strncmp(general_id.c_str(), temp, general_id.length())) {
				delete[] temp;
				return false;
			}
			delete[] temp;
			return true;
		}
		void mesh::load(mesh_section & parent, int currentLevel)
		{
			auto type_section = get_type_section();
			while (!file.eof()) {
				switch (type_section) {
				case Resources::TypeMeshSection::Element:
					load_element(parent);
					break;
				case Resources::TypeMeshSection::Section:
					int level = get_level_section();
					if (currentLevel >= level) {
						for (int i = 0; i < level; i++)
							file.unget();
						return;
					}
					else {
						auto name_section = get_name_section();
						unsigned id = get_uniqueId();
						parent.set_section(name_section, id);
						load(parent.get_section(id), currentLevel + 1);
						break;
					}
					break;
				}
				type_section = get_type_section();
			}
		}

		void mesh::load_element(mesh_section &section)
		{
			auto name_element = get_name_element();
			auto type = get_type_element();
			int size_array = get_int_sizes();
			switch (type) {
			case Resources::TypeMeshElement::Int: {
				auto arr = get_int_elements(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			case Resources::TypeMeshElement::Float: {
				auto arr = get_float_elements(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			case Resources::TypeMeshElement::String: {
				auto arr = get_value_strings(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			}
		}

		TypeMeshSection mesh::get_type_section()
		{
			char x;
			file.read(&x, 1);
			if (file.eof())
				return TypeMeshSection::None;
			switch (x) {
			case '!':
				return TypeMeshSection::Element;
				break;
			case '[':
				file.unget();
				return TypeMeshSection::Section;
				break;
			default:
				throw TRACE_EXCEPTION("incorrect parse meshFile: " + get_name_file());
			}
		}
		TypeMeshElement mesh::get_type_element()
		{
			char x;
			file.read(&x, 1);
			switch (x) {
			case 'i':
				return TypeMeshElement::Int;
				break;
			case 'f':
				return TypeMeshElement::Float;
				break;
			case 's':
				return TypeMeshElement::String;
				break;
			default:
				throw TRACE_EXCEPTION("incorrect parse meshFile: " + get_name_file());
			}
		}
		std::string mesh::get_name_section()
		{
			char buffer[512];
			int size = 0;
			for (char currentSymb = file.get(); currentSymb != '\0'; size++, currentSymb = file.get()) {
				if (size >= 512)
					throw TRACE_EXCEPTION("Out of range");
				buffer[size] = currentSymb;
			}
			buffer[size] = '\0';
			return string(buffer);
		}
		std::string mesh::get_name_element()
		{
			char buffer[256];
			auto size = get_bytes_size();
			if (size >= 256)
				throw TRACE_EXCEPTION("Value string >= 256 symbols");
			file.read(buffer, size);
			buffer[size] = '\0';
			return string(buffer);
		}
		std::string *mesh::get_value_strings(int count)
		{
			char buffer[1024];
			std::string *buffer_str = new std::string[count];
			for (int i = 0; i < count; ++i) {
				int size = get_int_sizes();
				if (size >= 1024)
					throw TRACE_EXCEPTION("Value string >= 1024 symbols");
				file.read(buffer, size);
				buffer_str[i] = std::string(buffer);
			}
			return buffer_str;
		}
		size_t mesh::get_bytes_size()
		{
			char object;
			file.read(reinterpret_cast<char	*>(&object), sizeof(char));
			return object;
		}
		size_t mesh::get_int_sizes()
		{
			int object;
			file.read(reinterpret_cast<char	*>(&object), sizeof(int));
			return object;
		}
		float *mesh::get_float_elements(int count)
		{
			float *arr = new float[count];
			file.read(reinterpret_cast<char*>(arr), sizeof(float) * count);
			return arr;
		}
		int *mesh::get_int_elements(int count)
		{
			int *arr = new int[count];
			file.read(reinterpret_cast<char*>(arr), sizeof(int) * count);
			return arr;
		}
		int mesh::get_level_section()
		{
			int count = 0;
			for (char currentSymb = file.get(); currentSymb == '['; count++, currentSymb = file.get());
			file.unget();
			return count;
		}
	}
}