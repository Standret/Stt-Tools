#pragma once
#include <exception>

#define TRACE_INFO __FUNCTION__, __FILE__, __LINE__

namespace exception
{
	class TraceException : public std::exception
	{
	public:
		TraceException(const std::string &message, const char *funct, 
			const char *file, int line) : std::exception(message.c_str()), funct_name(funct), file_name(file), line(line)
		{
			init_message();
		}
		char const *what() const override
		{
			return e_what_message.c_str();
		}
		inline std::string get_functName()
		{
			return funct_name;
		}
		inline std::string get_fileName()
		{
			return file_name;
		}
		inline int get_line()
		{
			return line;
		}
	private:
		void init_message()
		{
			file_name.erase(0, file_name.rfind("\\") + 1);
			e_what_message = (boost::format("[%s:%d]<%s()> %s") % file_name % line % funct_name % std::exception::what()).str();
		}
		std::string funct_name;
		std::string file_name;
		int line;
		std::string e_what_message;
	};

	class NotImplementException : public TraceException
	{
	public:
		NotImplementException(const char *funct, const char *file, int line) : 
			TraceException("not implement exception", funct, file, line) { }
	};
}