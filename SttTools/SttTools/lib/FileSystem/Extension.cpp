#include "Extension.h"

namespace File
{
	vector<string> Extension::registerExtensions;

	void Extension::RegisterExtension(string extension)
	{
		if (IsRegisterExtension(extension)) return;
		registerExtensions.push_back(extension);
	}

	void Extension::UnRegisterExtension(string extension)
	{
		for (int i = 0; i < registerExtensions.size(); i++)
			if (registerExtensions[i] == extension)
				registerExtensions.erase(registerExtensions.begin() + i);
	}

	bool Extension::IsRegisterExtension(string extension)
	{
		for (auto temp : registerExtensions)
			if (temp == extension)
				return true;
		return false;
	}
	string Extension::GetExtension(Resources::Extension extension)
	{
		switch (extension) {
		case Resources::Extension::Mesh:
			return "mesh";
		default:
			throw std::runtime_error("unkown type GetExtension(Resources::Extension extension");
		}
	}
}