#pragma once

#include <list>
#include <iostream>
#include <iomanip>

#include "Extension.h"

namespace File
{
	using std::list;
	using std::string;
	using std::cout;
	using std::endl;
	using std::setw;
	namespace fs = boost::filesystem;

	class Directory
	{
	public:
		Directory();
		Directory(const fs::path path);
		Directory(const string path);
		Directory(const char *path);
		bool ExistsFile(string name);
		void AddNewElement(fs::path path);
		void NewPath(const fs::path path);
		void NewPath(const string path);
		void NewPath(const char *path);
		void RenameNotChange(const string oldName, const string newName);
		fs::path GetPath(string name);
		string GetName(fs::path path);
		string GetExtension(string name);
		string GetExtension(fs::path path);
		unsigned int GetSize(string name);
		unsigned int GetSize(fs::path path);
		int GetFullLenght(fs::path path);
		int GetNameLeght(fs::path path);
		void PrintExtension(string extension);
		void PrintAllPaths();
		void PrintPathCurrentDirectory();
		void Up();
		void GoToRoot();
		void GoTo(string path);
	private:
		void InitPaths();
		int MaxNameLenght;
		fs::path currentDirectory;
		list<fs::path> filePaths;
		list<fs::path> directoryPaths;
	};
}
