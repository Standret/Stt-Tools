#pragma once

namespace Resources
{
	enum class BackgroundColor
	{
		Black, Blue, Green, Cyan, Red, Magenta, Brown,
		LightGray, DarkGray, LightBlue, LightGreen,
		LightCyan, LightRed, LightMagenta, Yellow, White
	};
	enum class ForegroundColor
	{
		Black, Blue, Green, Cyan, Red, Magenta, Brown,
		LightGray, DarkGray, LightBlue, LightGreen,
		LightCyan, LightRed, LightMagenta, Yellow, White
	};
	enum class Symbol
	{
		Python, Window, Linux, Git
	};

	struct Theme
	{
		BackgroundColor backgroundColor;
		ForegroundColor foregroundColor;
		Theme(BackgroundColor backgroundColor, ForegroundColor foregroundColor) :
			backgroundColor(backgroundColor), foregroundColor(foregroundColor) { }
	};
}