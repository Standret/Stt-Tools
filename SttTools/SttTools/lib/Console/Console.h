#pragma once

#include <Windows.h>
#include <string>
#include <iostream>

#include "ConsoleResources.h"

namespace Services
{
	using Resources::Theme;
	using Resources::BackgroundColor;
	using Resources::ForegroundColor;
	using Resources::Symbol;
	using std::string;
	using std::cin;
	using std::cout;

	static class Console
	{
	private:
		static Theme theme;
		static int count_call;
	public:
		// funct selector
		inline static BackgroundColor GetBackgroundColor() { return theme.backgroundColor; }
		inline static ForegroundColor GetForegroundColor() { return theme.foregroundColor; }
		inline static Theme GetConsoleTheme() { return theme; }
		// funct mutator
		static void SetBackgroundColor(BackgroundColor color);
		static void SetForegroundColor(ForegroundColor color);
		static void SetConsoleTheme(Theme _theme);
		// methods
		static void SetText(string text, Theme theme);
		static void SetText(string text, BackgroundColor color);
		static void SetText(string text, ForegroundColor color);
		static void SetErrorMessage(string message);
		static void SetWarningMessage(string message);
		static void SetErrorException(std::exception e);
		static void PutOutSymb(Symbol = Symbol::Python);
		static int GetCount()
		{
			return count_call;
		}
		static string GetName();
		static int GetInt();
		static void ClearBuffer();
	};
}