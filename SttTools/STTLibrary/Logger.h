#pragma once

//#include <boost/log/sources/severity_logger.hpp>
//#include <boost/log/sources/record_ostream.hpp>

#include <string>
#include <boost\log\core.hpp>
#include <boost\log\trivial.hpp>
#include <boost\log\expressions.hpp>
#include <boost\log\support\date_time.hpp>
#include <boost\log\utility\setup\file.hpp>
#include <boost\log\utility\setup\common_attributes.hpp>
#include <boost\date_time\posix_time\posix_time_types.hpp>


namespace logger
{
	namespace b_trivial = boost::log::trivial;
	namespace b_log = boost::log;
	namespace b_keywords = boost::log::keywords;
	namespace b_expr = boost::log::expressions;
	class Logging
	{
	public:
		static Logging &create_instance();
		void set_logLevel_LogFile(b_trivial::severity_level level);
		b_trivial::severity_level get_logLevel() const;
		void add_logFile(const std::string &name = "error.log");
		void write_message(const std::string &message, b_trivial::severity_level level = b_trivial::severity_level::warning);
	private:
		bool write_console;
		bool write_logFile;
		Logging();
		void init_logLevel();
		b_trivial::severity_level severite_level_LogFile;
		boost::log::sources::severity_logger<b_trivial::severity_level> lg;
	};
}