#pragma once

#include "Extensions.h"

#include <vector>

namespace File
{
	using std::string;
	using std::vector;
	using std::endl;

	class Extension
	{
	public:
		static void RegisterExtension(string extension);
		static void UnRegisterExtension(string extension);
		static bool IsRegisterExtension(string extension);
		static string GetExtension(Resources::Extension extension);
	private:
		string extension;
		bool isRegister;
		static vector<string> registerExtensions;
	};
}