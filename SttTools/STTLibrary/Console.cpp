#include "Console.h"

namespace Services
{
	int Console::count_call = 0;
	Theme Console::theme(BackgroundColor::Black, ForegroundColor::White);

	void Console::SetConsoleTheme(Theme _theme)
	{
		theme = _theme;
		HANDLE handle = GetStdHandle(STD_OUTPUT_HANDLE);
		SetConsoleTextAttribute(handle, (WORD)theme.backgroundColor << 4 | (WORD)theme.foregroundColor);
	}
	void Console::SetBackgroundColor(BackgroundColor color)
	{
		SetConsoleTheme(Theme(color, theme.foregroundColor));
	}
	void Console::SetForegroundColor(ForegroundColor color)
	{
		SetConsoleTheme(Theme(theme.backgroundColor, color));
	}
	void Console::SetText(string text, Theme theme)
	{
		Theme current = GetConsoleTheme();
		SetConsoleTheme(theme);
		std::cout << text;
		SetConsoleTheme(current);
	}
	void Console::SetText(string text, BackgroundColor color)
	{
		SetText(text, Theme(color, theme.foregroundColor));
	}
	void Console::SetText(string text, ForegroundColor color)
	{
		SetText(text, Theme(theme.backgroundColor, color));
	}
	void Console::SetErrorMessage(string message)
	{
		SetText("--Error: ", ForegroundColor::LightRed);
		std::cout << message << std::endl;
	}
	void Console::SetWarningMessage(string message)
	{
		SetText("--Warning: ", ForegroundColor::Yellow);
		std::cout << message << std::endl;
	}
	void Console::SetErrorException(std::exception e)
	{
		SetText("--Exception: ", ForegroundColor::LightRed);
		std::cout << e.what() << std::endl;
	}
	void Console::PutOutSymb(Symbol current)
	{
		string output;
		switch (current) {
		case Symbol::Window: output = "> "; break;
		case Symbol::Python: output = ">>> "; break;
		case Symbol::Linux: output = "~$ "; break;
		case Symbol::Git: output = "$ "; break;
		}
		cout << output;
	}
	void Console::ClearBuffer()
	{
		cin.unget();
		cin.ignore(300, '\n');
	}
	string Console::GetName()
	{
		count_call++;
		string temp;
		(cin >> temp).get();
		return temp;
	}
	int Console::GetInt()
	{
		count_call++;
		int temp;
		cin >> temp;
		if (!cin) {
			if (cin.bad()) throw 1;
			else if (cin.fail()) {
				cin.clear();
				cin.ignore(300, '\n');
				throw std::runtime_error("Error cin.fail()\n");
			}
			else if (cin.eof()) cin.clear();
		}
		return temp;
	}
}