#include "Logger.h"

namespace logger
{
	Logging::Logging() : write_console(false), write_logFile(false) {}

	void Logging::init_logLevel()
	{
		b_log::core::get()->set_filter
		(
			b_trivial::severity >= severite_level_LogFile
		);
	}

	Logging &Logging::create_instance()
	{
		static Logging s;
		return s;
	}

	inline void Logging::set_logLevel_LogFile(b_trivial::severity_level level)
	{
		severite_level_LogFile = level;
	}
	inline b_trivial::severity_level Logging::get_logLevel() const
	{
		return severite_level_LogFile;
	}

	void Logging::add_logFile(const std::string &name)
	{
		boost::log::add_file_log
		(
			b_keywords::file_name = name,
			b_keywords::rotation_size = 10 * 1024 * 1024,
			b_keywords::format = (b_expr::stream
				<< "[" << b_expr::format_date_time< boost::posix_time::ptime >("TimeStamp", "%H:%M:%S") << "]"
				<< "<" << b_log::trivial::severity << ">"
				<< b_expr::smessage)
		);
		write_logFile = true;
	}
	void Logging::write_message(const std::string &message, b_trivial::severity_level level)
	{
		b_log::add_common_attributes();
		BOOST_LOG_SEV(lg, level) << message;
	}
}