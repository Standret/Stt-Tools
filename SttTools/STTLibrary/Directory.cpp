#include "Directory.h"
#include "Console.h"

namespace File
{
	Directory::Directory()
	{
		NewPath(fs::current_path());
	}
	Directory::Directory(const fs::path path)
	{
		NewPath(path);
	}
	Directory::Directory(const string path)
	{
		NewPath(path);
	}
	Directory::Directory(const char *path)
	{
		NewPath(path);
	}

	bool Directory::ExistsFile(string name)
	{
		auto currentPath = currentDirectory.string();
		fs::path selectedPath(currentPath + "\\" + name);
		if (fs::exists(selectedPath) && !fs::is_directory(selectedPath))
			return true;
		return false;
	}

	void Directory::AddNewElement(const fs::path path)
	{
		if (MaxNameLenght < path.filename().string().length())
			MaxNameLenght = path.filename().string().length();
		if (fs::is_directory(path))
			directoryPaths.push_front(path);
		else
			filePaths.push_front(path);
	}
	void Directory::NewPath(const fs::path path)
	{
		if (!fs::exists(path)) throw std::runtime_error("Directory::NewPath(const fs::path path)");
		currentDirectory = path;
	}
	void Directory::NewPath(const string path)
	{
		NewPath(fs::path(path));
	}
	void Directory::NewPath(const char *path)
	{
		NewPath(fs::path(path));
	}

	void Directory::RenameNotChange(const string oldName, const string newName)
	{
		auto oldPath = GetPath(oldName);
		if (!fs::exists(oldPath)) throw std::runtime_error("File not found: " + oldName);
		auto newPath = GetPath(newName);
		if (fs::exists(newPath)) throw std::runtime_error("Path already exists: " + newName);
		fs::rename(oldPath, newPath);
	}

	fs::path Directory::GetPath(string name)
	{
		return fs::path(currentDirectory.string() + "\\" + name);
	}

	string Directory::GetName(fs::path path)
	{
		if (fs::is_directory(path))
			return path.filename().string();
		else {
			string fileName = path.filename().string(), fileExtension = path.extension().string();
			fileName.erase(fileName.length() - path.extension().string().size());
			return fileName;
		}
	}
	string Directory::GetExtension(string name)
	{
		return GetExtension(fs::path(GetName(currentDirectory) + name));
	}
	string Directory::GetExtension(fs::path path)
	{
		if (fs::is_directory(path)) throw std::runtime_error("path is folder;\nFunct: GetExtension(fs::path path);");
		string fileExtension = path.extension().string();
		fileExtension.erase(0, 1);
		return fileExtension;
	}
	unsigned int Directory::GetSize(string name)
	{
		return GetSize(fs::path(GetName(currentDirectory) + name));
	}
	unsigned int Directory::GetSize(fs::path path)
	{
		if (fs::is_directory(path)) throw std::runtime_error("path is folder;\nFunct: GetSize(fs::path path);");
		return fs::file_size(path);
	}
	int Directory::GetFullLenght(fs::path path)
	{
		return path.filename().string().length();
	}
	int Directory::GetNameLeght(fs::path path)
	{
		return path.filename().string().length();
	}

	void Directory::PrintExtension(string extension)
	{
		if (!File::Extension::IsRegisterExtension(extension)) {
			auto str = "<" + extension + ">";
			std::cout << str;
		}
		else
			Services::Console::SetText("<" + extension + ">", Services::ForegroundColor::LightGreen);
	}
	void Directory::PrintAllPaths()
	{
		InitPaths();
		for (auto directory : directoryPaths) {
			cout << setw(MaxNameLenght + 5) << std::left << GetName(directory) << setw(20) << "<dir>" << endl;
		}
		for (auto file : filePaths) {
			cout << setw(MaxNameLenght + 5) << std::left << GetName(file) << setw(20);
			PrintExtension(GetExtension(file)); cout << GetSize(file) / 1024 << "Kb" << endl;
		}
	}
	void Directory::PrintPathCurrentDirectory()
	{
		cout << currentDirectory.string() << std::endl;
	}

	void Directory::Up()
	{
		auto path = currentDirectory;
		auto rootPath = currentDirectory.root_path();
		if (path != rootPath) {
			auto parentPath = currentDirectory.parent_path();
			NewPath(parentPath);
		}
	}
	void Directory::GoToRoot()
	{
		NewPath(currentDirectory.root_path());
	}
	void Directory::GoTo(string path)
	{
		auto currentPath = currentDirectory.string();
		auto selectedPath = currentPath + "\\" + path;
		try {
			NewPath(selectedPath);
		}
		catch (std::exception e) {
			Services::Console::SetErrorMessage("System cannot find path: " + path);
			Services::Console::SetErrorException(e);
		}
	}
	void Directory::InitPaths()
	{
		filePaths.clear();
		directoryPaths.clear();
		MaxNameLenght = 0;
		fs::directory_iterator iterator(currentDirectory);
		for (auto temp : iterator) {
			if (MaxNameLenght < temp.path().filename().string().length())
				MaxNameLenght = temp.path().filename().string().length();
			if (fs::is_directory(temp))
				directoryPaths.push_front(temp.path());
			else
				filePaths.push_front(temp.path());
		}
	}
}