﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Model
{
    public static class Constants
    {
        public const string GeneralRegion = "Shell";

        public const string SettingsFileName = "GeneralSettings.settings";
        public const string TitleWindowCreateProjecr = "New Project";
        public const int HeightWindowCreateProject = 650;
        public const int WidthWindowCreateProject = 900;
    }
}
