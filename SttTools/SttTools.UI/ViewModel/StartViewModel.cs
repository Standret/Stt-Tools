﻿using Microsoft.Practices.ServiceLocation;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.UI.Extensions;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.View;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SttTools.UI.ViewModel
{
    public class StartViewModel : BaseViewModel
    {
        private readonly IProjectService _projectService = ServiceLocator.Current.GetInstance<IProjectService>();

        private ObservableCollection<ProjectSettings> projects;
        public ObservableCollection<ProjectSettings> Projects
        {
            get => projects;
            set
            {
                projects = value;
                RaisePropertyChanged("Projects");
            }
        }

        private int projectSelected = -1;
        public int ProjectSelected
        {
            get { return projectSelected; }
            set
            {
                projectSelected = value;
                if (projectSelected >= 0 && Projects.Count > 0)
                    Navigate<SolutionExplorerViewModel, SolutionExplorerView, string>(Projects[projectSelected].FullPath, Regions.ExplorerMain);
                RaisePropertyChanged("ProjectSelected");
            }
        }

        public void SelectAndLoad()
        {
            Navigate<MeshDataStructureViewModel, MeshDataStructureView>(Regions.Main);
        }

        protected override async Task Load()
        {
            Projects = (await _projectService.GetSettingProject()).ToObservable();
        }
    }
}
