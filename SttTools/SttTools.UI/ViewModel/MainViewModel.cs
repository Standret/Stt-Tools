﻿using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using SttTools.APIModule;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.ResponseModel;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.Modules.Rest;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SttTools.UI.ViewModel
{
    public class LoginApiModule
    {
        public string access_token { get; set; }
        public string token_type { get; set; }
        public int expires_in { get; set; }
        public string refresh_token { get; set; }
        public string userName { get; set; }
        public string routeId { get; set; }
        public string roles { get; set; }
        public string fullName { get; set; }
        public string isDeleted { get; set; }
        public string isBlocked { get; set; }
        public string imagePath { get; set; }
        public string id { get; set; }
        [JsonProperty(PropertyName = ".issued")]
        public DateTime issued { get; set; }
        [JsonProperty(PropertyName = ".expires")]
        public DateTime expires { get; set; }
    }

    public class MainViewModel : BaseViewModel
    {
        private InteractionRequest<InteractionRequestCreateProject> _interactionRequest;
        private readonly IKernelService _kernelService;
        private readonly ISolutionExplorer _solutionExplorer;
        private readonly IWindowsManagerService _windowsManagerService;

        public ICommand StartInitializeBaseComponnent { get; set; }
        public ICommand ShowCreateProjectWindow { get; set; }
        public ICommand OpenProject { get; set; }
        public ICommand ShowToolsRequest { get; set; }
        public IInteractionRequest InteractionRequest
        {
            get { return _interactionRequest; }
        }


        public MainViewModel()
        {
            ServiceLocator.Current.GetInstance<IProjectService>();
            _solutionExplorer = ServiceLocator.Current.GetInstance<ISolutionExplorer>();
            _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();
      
            OpenProject = new DelegateCommand(OnOpenProject);
            //StartInitializeBaseComponnent = new DelegateCommand(() =>
            //{
            //    Navigate<StartViewModel, StartView>(Regions.General);
            //    Navigate<SolutionExplorerViewModel, SolutionExplorerView>(Regions.Explorer);
            //});
            RaisePropertyChanged(() => _solutionExplorer);
            _interactionRequest = new InteractionRequest<InteractionRequestCreateProject>();
        }

        protected override async Task Initialize()
        {
            
        }


        //private async void OnCreateProject(ProjectSettings settings)
        //{
        //    if (!Directory.Exists(settings.Location.GetFolderPath()))
        //        Directory.CreateDirectory(settings.Location.GetFolderPath());

        //    var result = await _solutionExplorer.InitializeSolutionCatalog(settings);
        //    if (!result)
        //    {
        //        MessageBox.Show("Could not initialize solution");
        //        return;
        //    }

        //  //  result = await _kernelService.RegisterProject(settings);
        //    if (!result)
        //        MessageBox.Show("Files not saved!");
        //}
#pragma warning disable CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        private async void OnOpenProject()
#pragma warning restore CS1998 // This async method lacks 'await' operators and will run synchronously. Consider using the 'await' operator to await non-blocking API calls, or 'await Task.Run(...)' to do CPU-bound work on a background thread.
        {
            //var res = _windowsManagerService.OpenDialogFileBrowser();
            //if (res.NameFile != null)
            //{
            // //   var result = await _kernelService.RegisterProject(new ProjectSettings { Location = res });
            //    if (!result)
            //    {
            //        MessageBox.Show("Files not saved!");
            //        return;
            //    }

            //    _solutionExplorer.Load(res);
            //}
        }
    }
}