﻿using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.ObjectModel;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SttTools.UI.ViewModel
{
    public class MeshDataStructureViewModel : BaseViewModel<string>
    {
        private readonly ISolutionExplorer _solutionExplorer;
        private readonly IMeshService _meshService;
        private readonly IWindowsManagerService _windowsManagerService;


        public ICommand Clear => new DelegateCommand(OnClear);
        private void OnClear()
        {
            ValueString = String.Empty;
        }

        public ICommand ReloadFile => new DelegateCommand(OnReloadFile);
        private void OnReloadFile()
        {
            Structure = _meshService.ReloadDocument();
        }
        public ICommand ReloadValue => new DelegateCommand(OnReloadValue);
        private void OnReloadValue()
        {
            LoadArrayData();
        }
        public ICommand SaveNode => new DelegateCommand(OnSaveNode);
        private void OnSaveNode()
        {
            var result = _meshService.ChangeNodeName(SelectedNodeName, SelectedItem.Id);
            if (!result)
                _windowsManagerService.ShowMessageBox("Unknown error when try change name this node", TypeMessage.Error);
        }
        public ICommand SaveFile => new DelegateCommand(OnSaveFile);
        private void OnSaveFile()
        {
            var result = _meshService.Save(saveMode1, saveMode2);
            if (!result)
                _windowsManagerService.ShowMessageBox("Unknown error when try save this file, maybe this file already using or deleted or program has not right accessibility", TypeMessage.Error);
        }


        #region Property
        public ObservableCollection<MeshNode> files;
        public ObservableCollection<MeshNode> Structure
        {
            get { return files; }
            set
            {
                files = value;
                RaisePropertyChanged("Structure");
            }
        }
        private ObservableCollection<MeshInfo> meshInfo;
        public ObservableCollection<MeshInfo> MeshInfo
        {
            get { return meshInfo; }
            set
            {
                meshInfo = value;
                RaisePropertyChanged("MeshInfo");
            }
        }

        private MeshNode selectedItem;
        public MeshNode SelectedItem
        {
            get { return selectedItem; }
            set
            {
                selectedItem = value;
                if (InsertingMaxValue)
                    MaxCountElement = value.Count;

                LoadArrayData();
                SelectedNodeName = value.Name;
                SelectedType = value.TypeSectionString;
                RaisePropertyChanged(() => SelectedItem);
            }
        }

        private string selectedNodeName;
        public string SelectedNodeName
        {
            get => selectedNodeName;
            set
            {
                selectedNodeName = value;
                RaisePropertyChanged("SelectedNodeName");
            }
        }

        private string selectedType;
        public string SelectedType
        {
            get => selectedType;
            set
            {
                selectedType = value;
                RaisePropertyChanged("SelectedType");
            }
        }


        private int maxCountElement;
        public int MaxCountElement
        {
            get => maxCountElement;
            set
            {
                if (value < 0)
                    value = 0;
                if (value > selectedItem?.Count)
                {
                    value = selectedItem.Count;
                    LoadArrayData();
                }

                maxCountElement = value;
                RaisePropertyChanged("MaxCountElement");
            }
        }

        private bool insertingMaxValue = true;
        public bool InsertingMaxValue
        {
            get => insertingMaxValue;
            set
            {
                if (selectedItem != null)
                    MaxCountElement = SelectedItem.Count;
                insertingMaxValue = value;
                RaisePropertyChanged("InsertingMaxValue");
            }
        }

        private string valuesString;
        public string ValueString
        {
            get => valuesString;
            set
            {
                valuesString = value;
                RaisePropertyChanged("ValueString");
            }
        }

        private string fillName;
        public string FileName
        {
            get { return fillName; }
            set
            {
                fillName = value;
                RaisePropertyChanged("FileName");
            }
        }
        private long fileSize;
        public long FileSize
        {
            get { return fileSize; }
            set
            {
                fileSize = value;
                RaisePropertyChanged("FileSize");
            }
        }
        private string extension;
        public string Extension
        {
            get { return extension; }
            set
            {
                extension = value;
                RaisePropertyChanged("Extension");
            }
        }
        private DateTime lastModified;
        public DateTime LastModified
        {
            get { return lastModified; }
            set
            {
                lastModified = value;
                RaisePropertyChanged("LastModified");
            }
        }


        private bool allowChanges;
        public bool AllowChanges
        {
            get => allowChanges;
            set
            {
                allowChanges = value;
                RaisePropertyChanged("AllowChanges");
            }
        }


        #region RadioButtonGroupSave
        private bool saveMode0 = true;
        public bool SaveMode0
        {
            get => saveMode0;
            set
            {
                saveMode0 = value;
                RaisePropertyChanged("SaveMode0");
            }
        }
        private bool saveMode1;
        public bool SaveMode1
        {
            get => saveMode1;
            set
            {
                saveMode1 = value;
                RaisePropertyChanged("SaveMode1");
            }
        }
        private bool saveMode2;
        public bool SaveMode2
        {
            get => saveMode2;
            set
            {
                saveMode2 = value;
                RaisePropertyChanged("SaveMode2");
            }
        }
        #endregion

        #endregion

        public MeshDataStructureViewModel()
        {
            KeepAlive = true;
            _solutionExplorer = ServiceLocator.Current.GetInstance<ISolutionExplorer>();
            _meshService = ServiceLocator.Current.GetInstance<IMeshService>();
            _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();
        }

        protected override async Task Load(string parametr)
        {
            if (parametr == null)
                return;

            if (String.IsNullOrWhiteSpace(parametr) || !File.Exists(parametr))
                _windowsManagerService.ShowMessageBox(String.Format("Sorry path {0} is not valid, please restart solution", parametr), TypeMessage.Error);
            else
            {
                Structure = _meshService.LoadDocument(parametr);
                if (Structure != null)
                {
                    MeshInfo = _meshService.GetMeshInfo();
                    LoadFileInfo(parametr);
                }
            }
        }

        private void DonwloadDataStructure(string path)
        {
            Structure = _meshService.LoadDocument(path);
        }

        private void LoadFileInfo(string path)
        {
            var file = new FileInfo(path);
            FileName = file.Name;
            Extension = file.Extension.Substring(1);
            FileSize = file.Length;
            LastModified = file.LastWriteTime;
        }

        private void LoadArrayData()
        {
            if (selectedItem == null)
                return;
            Task.Run(() =>
            {
                int countElement = MaxCountElement > selectedItem.Count ? selectedItem.Count : MaxCountElement;
                var result = _meshService.GetValues(selectedItem.TypeSection, selectedItem.Id, countElement);
                InvokeOnMainThread(() => ValueString = result);
            });
        }
    }
}