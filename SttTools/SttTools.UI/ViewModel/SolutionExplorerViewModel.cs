﻿using Microsoft.Practices.ServiceLocation;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.View;
using System;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SttTools.UI.ViewModel
{
    public class SolutionExplorerViewModel : BaseViewModel<string>
    {
        private readonly ISolutionExplorer _solutionExplorerService = ServiceLocator.Current.GetInstance<ISolutionExplorer>();

        private ObservableCollection<NodeCatalog> files;
        public ObservableCollection<NodeCatalog> Files
        {
            get { return files; }
            set
            {
                files = value;
                RaisePropertyChanged("Files");
            }
        }

        public static event Action<string> Selected;

        private NodeCatalog selectedItem;
        public NodeCatalog SelectedItem
        {
            get { return selectedItem; }
            set
            {
                if (!String.IsNullOrWhiteSpace(value.Extension) && Extensions.Extensions.IsRegisterExtension(value.Extension))
                {
                    Navigate<MeshDataStructureViewModel, MeshDataStructureView, string>(value.FullPath, Regions.Main);
                }
                selectedItem = value;
            }
        }

        public SolutionExplorerViewModel()
        {
            KeepAlive = true;
        }

        protected override async Task Load(string parametr)
        {
            Files = await _solutionExplorerService.GetSolutionCatalogs(parametr);
        }

        //public SolutionExplorerViewModel()
        //{
        //    _solutionExplorer = ServiceLocator.Current.GetInstance<ISolutionExplorer>();
        //    _solutionExplorer.LoadSolution += async (l) =>
        //    {
        //       // Navigate(Regions.General, Views.MeshDataStructure);
        //        Files = await _solutionExplorer.GetSolutionCatalogs(l);
        //    };
        //}
    }
}
