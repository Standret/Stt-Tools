﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using SttTools.Domain.Entities.Enum.CustomPopups;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SttTools.UI.ViewModel.CustomPopups
{
    public class CustomConfirmaton : Confirmation
    {
        public Buttons ShowButton { get; set; }
        public Buttons ClickOnButton { get; set; }
        public string Description { get; set; }
    }

    public class ConfirmationPopupViewModel : BaseViewModel, IInteractionRequestAware
    {

        public ICommand Click => new DelegateCommand<Buttons?>(OnClick);
        private void OnClick(Buttons? button)
        {
            if (!button.HasValue)
                return;

            customConfirmaton.ClickOnButton = button.Value;
            FinishInteraction?.Invoke();
        }

        private CustomConfirmaton customConfirmaton;
        public INotification Notification
        {
            get => customConfirmaton;
            set
            {
                customConfirmaton = value as CustomConfirmaton;
                Load();
            }
        }
        public Action FinishInteraction { get; set; }

        private Visibility okVisibility;
        public Visibility OkVisibility
        {
            get => okVisibility;
            set
            {
                okVisibility = value;
                RaisePropertyChanged("OkVisibility");
            }
        }
        private Visibility noVisibility;
        public Visibility NoVisibility
        {
            get => noVisibility;
            set
            {
                noVisibility = value;
                RaisePropertyChanged("NoVisibility");
            }
        }
        private Visibility cancelVisibility;
        public Visibility CancelVisibility
        {
            get => cancelVisibility;
            set
            {
                cancelVisibility = value;
                RaisePropertyChanged("CancelVisibility");
            }
        }

        private string description;
        public string Desription
        {
            get => description;
            set
            {
                description = value;
                RaisePropertyChanged("Desription");
            }
        }

        protected override Task Load()
        {
            OkVisibility = NoVisibility = CancelVisibility = Visibility.Collapsed;
            if (customConfirmaton.ShowButton.HasFlag(Buttons.Yes))
                OkVisibility = Visibility.Visible;
            if (customConfirmaton.ShowButton.HasFlag(Buttons.No))
                NoVisibility = Visibility.Visible;
            if (customConfirmaton.ShowButton.HasFlag(Buttons.Cancel))
                CancelVisibility = Visibility.Visible;

            Desription = customConfirmaton.Description;
            return base.Load();
        }




    }
}
