﻿using Prism.Interactivity.InteractionRequest;
using SttTools.Domain.Entities.Models.CustomPopups;
using System;
using System.Threading.Tasks;

namespace SttTools.UI.ViewModel.CustomPopups
{
    public class LoadingDataNotification : Notification
    {
        public Action<Action> Action;
        public ProgressBarInformation Information { get; set; }
    }

    public class LoaderPopupViewModel : BaseViewModel, IInteractionRequestAware
    {
        private LoadingDataNotification requestModel;
        public INotification Notification
        {
            get => requestModel;
            set
            {
                requestModel = value as LoadingDataNotification;
                Load();
            }
        }

        private ProgressBarInformation steepValue;
        public ProgressBarInformation SteepValue
        {
            get => steepValue;
            set
            {
                steepValue = value;
                RaisePropertyChanged("SteepValue");
            }
        }
        public Action FinishInteraction { get; set; }

        protected override Task Load()
        {
            SteepValue = requestModel.Information;
            requestModel.Action?.Invoke(() =>
            {
                if (SteepValue.Current == SteepValue.Total - 1)
                    InvokeOnMainThread(() => FinishInteraction?.Invoke());
                else
                    SteepValue.Current++;
            });
            return base.Load();
        }
    }
}