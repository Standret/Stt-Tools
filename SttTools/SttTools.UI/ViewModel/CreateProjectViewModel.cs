﻿using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.TemplateView;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SttTools.UI.ViewModel
{
    public class InteractionRequestCreateProject : Confirmation
    {
        public LocationPath Location { get; set; }

        public bool IsCancel { get; set; }
    }

    public class CreateProjectViewModel : BaseViewModel, IInteractionRequestAware
    {
        private readonly IWindowsManagerService _windowsManagerService;
        private readonly ITemplateService _templatesService;

        #region Property
        private TemplateProject selectedTemplates;
        public TemplateObjects SelectedTemplate
        {
            set
            {
                var res = _templatesService.GetTemplateProject(value?.Id ?? -1);
                Location.ExtensionFile = res.ExtensionsProjectFile;
                Location.UniqueProjectFolder = res.CreateDirectoryForProject;
                IsCreateDirectory = res.CreateDirectoryForProject;
                IsCreateBaseArchitecture = res.CreateBaseArchitecture;

                if (String.IsNullOrEmpty(ProjectName) || selectedTemplates == null || ProjectName == selectedTemplates.DefaultProjectName)
                    SetLocationProject(res.DefaultProjectName, Location.BasePath);
                
                selectedTemplates = res;
            }
        }

        public ICommand Ok => new DelegateCommand(OnOk);
        public ICommand Cancel => new DelegateCommand(OnClose);
        public ICommand Browse => new DelegateCommand(() => SetLocationProject(ProjectName, _windowsManagerService.OpenDialogFolderBrowser()));

        private bool isCreateDirectory = true;
        public bool IsCreateDirectory
        {
            get { return isCreateDirectory; }
            set
            {
                isCreateDirectory = value;
                Location.UniqueProjectFolder = value;
                RaisePropertyChanged("Location");
                RaisePropertyChanged("IsCreateDirectory");
            }
        }
        private bool isCreateBaseArchitecture;
        public bool IsCreateBaseArchitecture
        {
            get { return isCreateBaseArchitecture; }
            set
            {
                isCreateBaseArchitecture = value;
                RaisePropertyChanged("IsCreateBaseArchitecture");
            }
        }

        private string projectName;
        public string ProjectName
        {
            get { return projectName; }
            set
            {
                projectName = value;
                Location.NameFile = value;
                RaisePropertyChanged("SelectedType");
                RaisePropertyChanged("ProjectName");
                RaisePropertyChanged("Location");
            }
        }
        private LocationPath location;
        public LocationPath Location
        {
            get { return location; }
            set
            {
                location = value;
                RaisePropertyChanged("Location");
            }
        }

        // binding to template
        private List<TemplatesCreateObjectsViewModel> templatesProjects;
        public List<TemplatesCreateObjectsViewModel> TemplatesProjects
        {
            get { return templatesProjects; }
            set
            {
                templatesProjects = value;
                RaisePropertyChanged("TemplatesProjects");
            }
        }

        private InteractionRequestCreateProject requestModel;
        public INotification Notification
        {
            get { return requestModel; }
            set
            {
                requestModel = value as InteractionRequestCreateProject;
                requestModel.IsCancel = true;
                Load();
                RaisePropertyChanged(() => Notification);
            }
        }
        public Action FinishInteraction { get; set; }
        #endregion

        public CreateProjectViewModel()
        {
            _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();
            _templatesService = ServiceLocator.Current.GetInstance<ITemplateService>();

            Location = new LocationPath();
            SetLocationProject("Project");
        }

        protected override Task Load()
        {
            TemplatesProjects = _templatesService.Templates;
            Location = new LocationPath();
            ProjectName = null;
            SetLocationProject("Project");
            return base.Load();
        }

        private void SetLocationProject(string projectName, string path = null)
        {
            if (String.IsNullOrWhiteSpace(path))
                path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments); // todo: Change to last selected path

            if (!Directory.Exists(path))
                return;

            var folders = Directory.EnumerateDirectories(path).ToList();

            int i = 0;
            for (; i < Int32.MaxValue; ++i)
            {
                var name = path + "\\" + projectName + (i == 0 ? String.Empty : i.ToString());
                if (folders.Contains(name)) continue;
                else break;
            }

            Location.BasePath = path;
            ProjectName = Location.NameFile = projectName + (i == 0 ? String.Empty : i.ToString());
        }

        private void OnClose()
        {
            requestModel.IsCancel = true;
            FinishInteraction?.Invoke();
        }
        private void OnOk()
        {
            if (String.IsNullOrWhiteSpace(Location.NameFile))
            {
                MessageBox.Show("Empty project name");
                return;
            }
            requestModel.IsCancel = false;
            requestModel.Location = Location;

            FinishInteraction?.Invoke();
        }
    }
}
