﻿using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Enum.CustomPopups;
using SttTools.Domain.Entities.Models.CustomPopups;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.View;
using SttTools.UI.ViewModel.CustomPopups;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SttTools.UI.ViewModel.Menu
{
    public class TopMenuViewModel : BaseViewModel
    {
        private InteractionRequest<InteractionRequestCreateProject> _interactionRequestCreateProject;
        public IInteractionRequest InteractionRequestCreateProject { get { return _interactionRequestCreateProject; } }
        private InteractionRequest<CustomConfirmaton> _interactionRequestShowWarning;
        public IInteractionRequest InteractionRequestShowWarning { get { return _interactionRequestShowWarning; } }

        private InteractionRequest<LoadingDataNotification> _interactionRequestLoader = new InteractionRequest<LoadingDataNotification>();
        public IInteractionRequest InteractionRequestLoader { get { return _interactionRequestLoader; } }

        private readonly ISolutionExplorer _solutionExplorer;
        private readonly IWindowsManagerService _windowsManagerService;
        private readonly IProjectService _projectService = ServiceLocator.Current.GetInstance<IProjectService>();

        public ICommand ShowCreateProjectWindow { get; set; }
        public ICommand OpenProject { get; set; }

        public TopMenuViewModel()
        {
            _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();
            _solutionExplorer = ServiceLocator.Current.GetInstance<ISolutionExplorer>();
            _interactionRequestCreateProject = new InteractionRequest<InteractionRequestCreateProject>();
            _interactionRequestShowWarning = new InteractionRequest<CustomConfirmaton>();

            ShowCreateProjectWindow = new DelegateCommand(OnCreateProject);
            OpenProject = new DelegateCommand(OnOpenProject);
        }

        private void OnShowLoader(List<string> descriptionsSteep, string title, Action<Action> someAction)
        {
            _interactionRequestLoader.Raise(new LoadingDataNotification
            {
                Title = title,
                Information = new ProgressBarInformation(descriptionsSteep),
                Action = someAction
            }, r => Debug.WriteLine("-> Loading completed"));
        }
        private void OnCreateProject()
        {
            _interactionRequestCreateProject.Raise(new InteractionRequestCreateProject { Title = "Create Project" }, OnCreateNewProject);
        }
        private Buttons OnShowWarning(string description, Buttons showButtons)
        {
            Buttons result = Buttons.Unkown;
            _interactionRequestShowWarning.Raise(new CustomConfirmaton
            {
                ShowButton = showButtons,
                Title = "Warnings",
                Description = description
            }, r => result = r.ClickOnButton);
            return result;
        }

        private void OnCreateNewProject(InteractionRequestCreateProject model)
        {
            if (model.IsCancel)
                return;

            if (File.Exists(model.Location.GetFilePath()))
            {
                var resultClick = OnShowWarning("Project already exists. Overwrite files project?", Buttons.Yes | Buttons.No);
                if (resultClick == Buttons.No)
                    return;
            }

            OnShowLoader(new List<string>
            {
                "Initialize solution catalog",
                "Registering project",
                "Loading project"
            }, "Creating projects file",
            act =>
            {
                Task.Run(async () =>
                {
                    var result = await _solutionExplorer.InitializeSolutionCatalog(model.Location);
                    if (!result)
                    {
                        MessageBox.Show("Could not initialize solution");
                        return;
                    }
                    act?.Invoke();
                    _projectService.RegisterProject(model.Location.GetFilePath());
                    act?.Invoke();
                    act?.Invoke();

                    InvokeOnMainThread(() =>
                    {
                        _navigationModule.Navigate<MeshDataStructureViewModel, MeshDataStructureView>(Regions.Main);
                        _navigationModule.Navigate<SolutionExplorerViewModel, SolutionExplorerView, string>( model.Location.GetFilePath(), Regions.ExplorerMain);
                    });
                });
            });
        }

        private async void CreateProjects()
        {
          
        }

        private void OnOpenProject()
        {
            var res = _windowsManagerService.OpenDialogFileBrowser();
            //if (res.NameFile != null)
            //{
            //     // var result = await _kernelService.RegisterProject(new ProjectSettings { Location = res });
            //    if (!result)
            //    {
            //        MessageBox.Show("Files not saved!");
            //        return;
            //    }

            //    _solutionExplorer.Load(res);
            //}
        }
    }
}
