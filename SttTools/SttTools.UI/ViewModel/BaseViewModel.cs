﻿using Microsoft.Practices.ServiceLocation;
using Prism.Mvvm;
using Prism.Regions;
using SttTools.Domain.Entities.Enum;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SttTools.UI.ViewModel
{
    public abstract class BaseViewModel : BindableBase, INavigationAware, IRegionMemberLifetime
    {
        protected readonly INavigationModule _navigationModule;
        public bool KeepAlive { get; protected set; }

        public BaseViewModel()
        {
            _navigationModule = ServiceLocator.Current.GetInstance<INavigationModule>();
            Task.Run(Initialize);
        }

        protected virtual async Task Initialize() { }

        bool INavigationAware.IsNavigationTarget(NavigationContext navigationContext)
        {
            return KeepAlive;
        }
        void INavigationAware.OnNavigatedFrom(NavigationContext navigationContext)
        {
            Task.Run(Unload);
        }
        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            Task.Run(Load);
        }

        protected virtual async Task Load() { }
        protected virtual async Task Unload() { }

        protected void InvokeOnMainThread(Action action)
        {
            System.Windows.Application.Current.Dispatcher.Invoke(action);
        }

        protected void RaisePropertyChanged<T>(Expression<Func<T>> propertyExpression)
        {
            RaisePropertyChanged(((MemberExpression)propertyExpression.Body).Member.Name);
        }

        protected virtual void Navigate<TViewModel, TView>(Regions region = Regions.Shell)
            where TViewModel : BaseViewModel
            where TView : UserControl
        {
            _navigationModule.Navigate<TViewModel, TView>(region);
        }
        protected virtual void Navigate<TViewModel, TView, TParametr>(TParametr parametr, Regions region = Regions.Shell)
            where TViewModel : BaseViewModel<TParametr>
            where TView : UserControl
            where TParametr : class
        {
            _navigationModule.Navigate<TViewModel, TView, TParametr>(parametr, region);
        }

        private string FormatRegion(Regions region)
        {
            return region.ToString() + "Region";
        }
        private string FormatView(Views view)
        {
            return view.ToString() + "View";
        }
    }

    public abstract class BaseViewModel<TParametr> : BaseViewModel, INavigationAware 
        where TParametr : class
    {
        void INavigationAware.OnNavigatedTo(NavigationContext navigationContext)
        {
            var param = navigationContext.Parameters["param"] as TParametr;
            Task.Run(async () => await Load(param));
        }

        protected virtual async Task Load(TParametr parametr) { }
    }
}
