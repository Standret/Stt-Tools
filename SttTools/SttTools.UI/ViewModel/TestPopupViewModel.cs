﻿using Prism.Commands;
using Prism.Interactivity.InteractionRequest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SttTools.UI.ViewModel
{
    public class TestPopupViewModel : BaseViewModel, IInteractionRequestAware
    {
        public ICommand Ok { get; set; }
        public ICommand Cancel { get; set; }

        private ItemSelectionNotification notification;

        public TestPopupViewModel()
        {
            Ok = new DelegateCommand(() =>
            {
                notification.SelectedItem = "Ok";
                notification.Confirmed = true;

                FinishInteraction.Invoke();
            });

            Cancel = new DelegateCommand(() =>
            {
                notification.SelectedItem = "Cancel";
                notification.Confirmed = false;

                FinishInteraction.Invoke();
            });
        }

        public INotification Notification
        {
            get
            {
                return this.notification;
            }
            set
            {
                notification = value as ItemSelectionNotification;
            }
        }

        public Action FinishInteraction { get; set; }
    }

    public class ItemSelectionNotification : Confirmation
    {
        public ItemSelectionNotification()
        {
            
        }

        public string SelectedItem { get; set; }
    }
}
