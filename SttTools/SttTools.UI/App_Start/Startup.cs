﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SttTools.UI.App_Start
{
    class Startup
    {
        [STAThread]
        public static void Main(string[] args)
        {
            try
            {
                MemoryCache.CommandLineArgs = args;

                App ob = new App();
                ob.Run();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message + " |--| " + e.StackTrace);
                if (e.InnerException != null)
                    MessageBox.Show(e.InnerException.Message + " -- " + e.InnerException.StackTrace);
            }
        }
    }
}
