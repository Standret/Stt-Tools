﻿using System.Windows;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.Modules;
using Microsoft.Practices.Unity;
using SttTools.UI.App_Start;
using SttTools.UI.Extensions;
using System;
using Prism.Unity;
using Prism.Modularity;
using SttTools.ComputationalTools.Converter;
using System.Threading.Tasks;
using Microsoft.Practices.ServiceLocation;

namespace SttTools.UI.App_Start
{
    class Bootstrapper : UnityBootstrapper
    {
        public static MainWindow MainWindowShell { get; private set; }
        protected override DependencyObject CreateShell()
        {
            var shell = Container.TryResolve<MainWindow>();
            MainWindowShell = shell;
            return shell;
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<INavigationModule, NavigationModule>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IWindowsManagerService, WindowsManagerService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IKernelService, KernelService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ITemplateService, TemplateService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IProjectService, ProjectService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<ISolutionExplorer, SolutionExplorer>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IMeshService, MeshService>(new ContainerControlledLifetimeManager());
        }

        protected override void ConfigureModuleCatalog()
        {
            var moduleCatalog = (ModuleCatalog)ModuleCatalog;
            moduleCatalog.AddModule(typeof(NavigationModule));
            moduleCatalog.AddModule(typeof(AppExtendStart));
        }
    }
}
