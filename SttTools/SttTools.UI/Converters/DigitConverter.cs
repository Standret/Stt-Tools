﻿using SttTools.Domain.Entities.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SttTools.UI.Converters
{
    [ValueConversion(typeof(string), typeof(int))]
    public class DigitConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                return -1;

            string str = value as string;

            int variable;
            Int32.TryParse(str, out variable);

            return variable;
        }
    }
}
