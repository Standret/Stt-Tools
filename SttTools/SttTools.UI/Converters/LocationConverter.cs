﻿using SttTools.Domain.Entities.Models;
using System;
using System.Globalization;
using System.Windows.Data;

namespace SttTools.UI.Converters
{
    [ValueConversion(typeof(LocationPath), typeof(string))]
    public class LocationConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            return value?.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
