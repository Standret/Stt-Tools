﻿using System;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Windows.Data;

namespace SttTools.UI.Converters
{
    [ValueConversion(typeof(string), typeof(string))]
    public class MeshShortValuesConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is string))
                return "unkown type";

            var str = value as string;
            if (string.IsNullOrEmpty(str))
                return String.Empty;

            StringBuilder text = new StringBuilder();
            text.Append("[");
            text.Append(str);
            var count = 100;
            text.Append("]");

            return text.ToString();
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
