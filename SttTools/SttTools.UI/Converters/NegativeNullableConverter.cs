﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SttTools.UI.Converters
{
    [ValueConversion(typeof(bool?), typeof(bool?))]
    public class NegativeNullableConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool?))
                return false;

            var val = (bool?)value;
            return val.HasValue ? !val.Value : false;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is bool?))
                return false;

            var val = (bool?)value;
            return val.HasValue ? !val.Value : false;
        }
    }
}
