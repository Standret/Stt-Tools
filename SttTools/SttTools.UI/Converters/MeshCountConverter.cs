﻿using System;
using System.Globalization;
using System.Windows.Data;

namespace SttTools.UI.Converters
{
    [ValueConversion(typeof(int), typeof(string))]
    public class MeshCountConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (!(value is int))
                return "Error input";

            int count = (int)value;
            if (count == 0)
                return String.Empty;
            else
                return String.Format("Count: {0}", count);
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            throw new NotImplementedException();
        }
    }
}
