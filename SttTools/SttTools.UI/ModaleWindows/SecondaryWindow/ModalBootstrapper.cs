﻿using Prism.Modularity;
using Prism.Unity;
using SttTools.ComputationalTools.Converter;
using SttTools.Domain.Entities.Models;
using SttTools.UI.App_Start;
using SttTools.UI.Modules;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SttTools.UI.ModuleWindows.SecondaryWindow
{
    public class ModalWindowsBootsrapper : UnityBootstrapper
    {
        private WindowData data;
        public ModalWindowsBootsrapper(WindowData data)
        {
            this.data = data;
        }

        protected override DependencyObject CreateShell()
        {
            var shell = Container.TryResolve<ModalWindow>();
            shell.Height = data.Height;
            shell.Width = data.Width;
            shell.Owner = Bootstrapper.MainWindowShell;
            shell.Title = data.Title;
            return shell;
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.Show();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
        }

        protected override void ConfigureModuleCatalog()
        {
            var moduleCatalog = (ModuleCatalog)ModuleCatalog;
            moduleCatalog.AddModule(typeof(InitializeConverterModule));
        }
    }
}
