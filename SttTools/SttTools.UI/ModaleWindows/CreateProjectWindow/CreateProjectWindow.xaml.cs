﻿using SttTools.Domain.Entities.ViewModels.CreateProject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace SttTools.UI.ModaleWindows.CreateProjectWindow
{
    /// <summary>
    /// Interaction logic for CreateProjectWindow.xaml
    /// </summary>
    public partial class CreateProjectWindow : Window
    {
        public CreateProjectWindow()
        {
            InitializeComponent();

            ((CreateProjectWindowViewModel)DataContext).CloseDialogWindow += () => Close();
        }

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            ((CreateProjectWindowViewModel)DataContext).ChangeSelectedNode((TemplatesCreateObjectsViewModel)e.NewValue);
        }
    }
}
