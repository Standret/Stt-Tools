﻿using Microsoft.Practices.ServiceLocation;
using Prism.Commands;
using Prism.Mvvm;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Input;

namespace SttTools.UI.ModaleWindows.CreateProjectWindow
{
    public class CreateProjectWindowViewModel : BindableBase
    {
        private readonly DelegateCommand _ok;
        private readonly DelegateCommand _cancel;
        private readonly DelegateCommand _browse;
        private readonly IWindowsManagerService _windowsManagerService;

#pragma warning disable CS0414 // The field 'CreateProjectWindowViewModel.CreateProject' is assigned but its value is never used
        public event Action<ProjectSettings> CreateProject = null;
#pragma warning restore CS0414 // The field 'CreateProjectWindowViewModel.CreateProject' is assigned but its value is never used
        public event Action CloseDialogWindow = null;

        #region Property
        public List<TemplatesCreateObjectsViewModel> Templates { get; set; }

        private List<TemplateProject> projects;
        public List<TemplateProject> Projects
        {
            get { return projects; }
            set
            {
                projects = value;
                RaisePropertyChanged("Projects");
            }
        }

        private int selectedIndex;
        public int SelectedIndex
        {
            get { return selectedIndex; }
            set
            {
                selectedIndex = value;
                IsCreateDirectory = Projects[value].CreateDirectoryForProject;
                IsCreateBaseArchitecture = Projects[value].CreateBaseArchitecture;
                Location.ExtensionFile = Projects[value].ExtensionsProjectFile;

                RaisePropertyChanged("SelectedIndex");
                RaisePropertyChanged("ProjectType");
            }
        }
        public TemplateProject ProjectType
        {
            get
            {
                IsCreateDirectory = Projects[SelectedIndex].CreateDirectoryForProject;
                IsCreateBaseArchitecture = Projects[SelectedIndex].CreateBaseArchitecture;
                Location.ExtensionFile = Projects[SelectedIndex].ExtensionsProjectFile;
                SetLocationProject(Projects[SelectedIndex].DefaultProjectName, Location?.BasePath);
                return Projects[SelectedIndex];
            }
        }

        public ICommand Ok
        {
            get { return _ok; }
        }
        public ICommand Cancel
        {
            get { return _cancel; }
        }
        public ICommand Browse
        {
            get { return _browse; }
        }

        private bool isCreateDirectory = true;
        public bool IsCreateDirectory
        {
            get { return isCreateDirectory; }
            set
            {
                isCreateDirectory = value;
                Location.UniqueProjectFolder = value;
                RaisePropertyChanged("Location");
                RaisePropertyChanged("IsCreateDirectory");
            }
        }
        private bool isCreateBaseArchitecture;
        public bool IsCreateBaseArchitecture
        {
            get { return isCreateBaseArchitecture; }
            set
            {
                isCreateBaseArchitecture = value;
                RaisePropertyChanged("IsCreateBaseArchitecture");
            }
        }

        private string projectName;
        public string ProjectName
        {
            get { return projectName; }
            set
            {
                projectName = value;
                Location.NameFile = value;
                RaisePropertyChanged("ProjectName");
                RaisePropertyChanged("Location");
            }
        }
        private LocationPath location;
        public LocationPath Location
        {
            get { return location; }
            set
            {
                location = value;
                RaisePropertyChanged("Location");
            }
        }
        #endregion

        public CreateProjectWindowViewModel()
        {
            _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();
            var templateService = ServiceLocator.Current.GetInstance<ITemplateService>();
            Templates = templateService.Templates;

            _ok = new DelegateCommand(OnOk);
            _cancel = new DelegateCommand(OnClose);
            _browse = new DelegateCommand(() => SetLocationProject(_windowsManagerService.OpenDialogFolderBrowser(), ProjectName));
            Location = new LocationPath();

            SetLocationProject("Project");
        }

        public void ChangeSelectedNode(TemplatesCreateObjectsViewModel selected)
        {
           // Projects = selected.Projects;
            RaisePropertyChanged("ProjectType");
        }

        private void SetLocationProject(string projectName, string path = null)
        {
            if (String.IsNullOrWhiteSpace(path))
                path = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);

            if (!Directory.Exists(path))
                return;

            var folders = Directory.EnumerateDirectories(path).ToList();

            int i = 0;
            for (; i < Int32.MaxValue; ++i)
            {
                var name = path + "\\" + projectName + (i == 0 ? String.Empty : i.ToString());
                if (folders.Contains(name)) continue;
                else break;
            }

            Location.BasePath = path;            
            ProjectName = Location.NameFile = projectName + (i == 0 ? String.Empty : i.ToString());
        }

        private void OnClose()
        {
            CloseDialogWindow?.Invoke();
        }
        private void OnOk()
        {
        //    if (String.IsNullOrEmpty(Location.NameFile))
        //    {
        //        MessageBox.Show("Empty project name");
        //        return;
        //    }
        //    var project = new ProjectSettings
        //    {
        //        Type = Projects[SelectedIndex].TypeProject,
        //        Location = Location
        //    };
        //    CreateProject?.Invoke(project);
        //    OnClose();
        }
    }
}