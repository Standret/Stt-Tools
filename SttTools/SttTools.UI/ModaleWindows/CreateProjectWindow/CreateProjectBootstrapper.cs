﻿using Prism.Unity;
using Microsoft.Practices.Unity;
using SttTools.UI.App_Start;
using SttTools.UI.Model;
using SttTools.UI.Modules;
using SttTools.UI.Modules.Abstract;
using System.Windows;
using System;
using SttTools.Domain.Entities.Models;

namespace SttTools.UI.ModaleWindows.CreateProjectWindow
{
    public class CreateProjectBootstrapper : UnityBootstrapper
    {
        public event Action<ProjectSettings> CreateProject = null;

        public CreateProjectBootstrapper() { }

        protected override DependencyObject CreateShell()
        {
            var shell = Container.TryResolve<CreateProjectWindow>();
            shell.Height = Constants.HeightWindowCreateProject;
            shell.Width = Constants.WidthWindowCreateProject;
            shell.Owner = Bootstrapper.MainWindowShell;
            shell.Title = Constants.TitleWindowCreateProjecr;
            shell.WindowStartupLocation = WindowStartupLocation.CenterOwner;
            ((CreateProjectWindowViewModel)shell.DataContext).CreateProject += CreateProject;
            return shell;
        }

        protected override void InitializeShell()
        {
            Application.Current.MainWindow = (Window)Shell;
            Application.Current.MainWindow.ShowDialog();
        }

        protected override void ConfigureContainer()
        {
            base.ConfigureContainer();
            Container.RegisterType<IWindowsManagerService, WindowsManagerService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IProjectService, ProjectService>(new ContainerControlledLifetimeManager());
            Container.RegisterType<IKernelService, KernelService>(new ContainerControlledLifetimeManager());
        }
    }
}
