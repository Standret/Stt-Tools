﻿using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SttTools.UI.Model.Enum;
using Microsoft.Practices.Unity;
using SttTools.UI.View;
using SttTools.UI.ViewModel;
using SttTools.UI.Model;
using Microsoft.Practices.ServiceLocation;
using Prism.Regions;
using SttTools.ComputationalTools.Converter.Views;
using SttTools.ComputationalTools.Converter.ViewModels;
using Prism.Unity;
using SttTools.Domain.Entities.Enum;
using System.Windows.Controls;
using SttTools.UI.View.Menu;

namespace SttTools.UI.Modules
{
    public class NavigationModule : INavigationModule
    {
        private readonly IRegionManager _regionManager;
        private readonly IUnityContainer _unityContainer;

        public NavigationModule(IRegionManager regionManager, IUnityContainer unityContainer)
        {
            _regionManager = ServiceLocator.Current.GetInstance<IRegionManager>();
            _unityContainer = ServiceLocator.Current.GetInstance<IUnityContainer>();
        }

        public void Initialize()
        {
            var view = _unityContainer.Resolve<MainView>();
            view.DataContext = _unityContainer.Resolve<MainViewModel>();
            _regionManager.Regions[Constants.GeneralRegion].Add(view);

            _unityContainer.RegisterTypeForNavigation<StartView>();
            _unityContainer.RegisterTypeForNavigation<SolutionExplorerView>();
            _unityContainer.RegisterTypeForNavigation<MainView>();
            _unityContainer.RegisterTypeForNavigation<MeshDataStructureView>();
            _unityContainer.RegisterTypeForNavigation<TopMenuView>();
        }

        public void Navigate<TViewModel, TView>(Regions region = Regions.Main)
            where TViewModel : class
            where TView : UserControl
        {
            Navigate<TViewModel, TView, object>(null, region);
        }

        public void Navigate<TViewModel, TView, TParametr>(TParametr parametr, Regions region = Regions.Main)
            where TViewModel : class
            where TView : UserControl
            where TParametr : class
        {
            string regionName = FormatRegion(region);
            var pageType = typeof(TView);
            //if (_regionManager.Regions[regionName].GetView(pageType.Name) == null)
            //{
            //    var view = _unityContainer.Resolve<TView>();
            //    if (view == null)
            //        throw new ArgumentException(String.Format("{0} can not be resolve", pageType.FullName));

            //    view.DataContext = view.DataContext ?? _unityContainer.Resolve<TViewModel>();
            //    _regionManager.Regions[regionName].Add(view);
            //}

            var parametrs = new NavigationParameters();
            if (parametr != null)
                parametrs.Add("param", parametr);
            _regionManager.Regions[regionName].RequestNavigate(pageType.Name, parametrs);
        }

        private string FormatRegion(Regions region)
        {
            return region.ToString() + "Region";
        }
    }
}
