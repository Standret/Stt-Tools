﻿using Microsoft.Practices.ServiceLocation;
using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms.ProjectModels;
using SttTools.Domain.Entities.Models.SpecialModel;
using SttTools.UI.Extensions;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace SttTools.UI.Modules
{
    public class SolutionExplorer : ISolutionExplorer
    {
        private readonly IKernelService _kernelService;
        private BaseSolutionSettings globalSettings;
        private ProjectSettings localSettings;
        private string basePath;

        public SolutionExplorer()
        {
            _kernelService = ServiceLocator.Current.GetInstance<IKernelService>();
            globalSettings = new BaseSolutionSettings();
        }

        public event Action<string> LoadSolution;

        public async Task<ObservableCollection<NodeCatalog>> GetSolutionCatalogs(string fullPath)
        {
            var originalProjects = await _kernelService.Load<MainProjectsFiles>(fullPath);
            return originalProjects.ConvertToNodes(fullPath);
        }

        public async Task<bool> InitializeSolutionCatalog(LocationPath path)
        {
            if (!Directory.Exists(path.GetFolderPath()))
                Directory.CreateDirectory(path.GetFolderPath());

            basePath = path.BasePath;
            var projects = new MainProjectsFiles();
            //        var result = await _kernelService.Save(setting.Location.GetFilePath(), globalSettings);
            Initialize(path.BasePath, projects);
            var result = await _kernelService.Save(path.GetFilePath(), projects, true);
            return result;
        }

        public void Load(LocationPath path)
        {
            LoadSolution?.Invoke(path.GetFilePath());
        }
        public void Load(string path)
        {
            LoadSolution?.Invoke(path);
        }

        //public async Task<ObservableCollection<NodeCatalog>> GetSolutionCatalogs(LocationPath setting)
        //{
        //    if (globalSettings?.Catalogs != null && localSettings?.Location.GetFilePath() == setting.GetFilePath())
        //        return globalSettings.Catalogs;
        //    else
        //    {
        //        globalSettings = await _kernelService.Load<BaseSolutionSettings>(setting.GetFilePath());
        //        return globalSettings.Catalogs;
        //    }
        //}
        //public async Task<ObservableCollection<NodeCatalog>> GetSolutionCatalogs(string setting)
        //{
        //    globalSettings = await _kernelService.Load<BaseSolutionSettings>(setting);
        //    return globalSettings.Catalogs;
        //}

        //public async Task<bool> InitializeSolutionCatalog(ProjectSettings setting)
        //{
        //    try
        //    {
        //        globalSettings = new BaseSolutionSettings();
        //        globalSettings.Catalogs = new ObservableCollection<NodeCatalog>();
        //        Initialize(setting.Location.GetFolderPath(), globalSettings.Catalogs);
        //        var result = await _kernelService.Save(setting.Location.GetFilePath(), globalSettings);
        //        if (result)
        //            LoadSolution?.Invoke(setting.Location.GetFilePath());
        //        return result;
        //    }
        //    catch(Exception ex)
        //    {
        //        return false;
        //    }
        //}

        //private string GetProjectExtension(TypeProject type)
        //{
        //    switch (type)
        //    {
        //        case TypeProject.HeartsOfIronIV:
        //            return "ecproj";
        //        default:
        //            return "unk";
        //    }
        //}
        private void Initialize(string path, MainProjectsFiles parent)
        {
            try
            {
                var folder = Directory.EnumerateDirectories(path).ToList();
                parent.Folders = new List<MainProjectsFiles>();
                parent.Files = new List<string>();
                foreach (var res in folder)
                {
                    var name = res.Substring(basePath.Length + 1);
                    if (name.IsDeniedFolder())
                        continue;

                    parent.Folders.Add(new MainProjectsFiles());
                    parent.Folders.Last().BasePath = name;
                    Initialize(res, parent.Folders.Last());
                }

                var files = Directory.EnumerateFiles(path).Select(i => i.Substring(i.LastIndexOf('\\') + 1)).ToList();
                parent.Files = files;

            }
            catch (Exception ex)
            {
                Debug.WriteLine("--> Exception IO Denied: " + ex.Message);
            }
        }
    }
}
