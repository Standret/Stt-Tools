﻿using Newtonsoft.Json;
using SttTools.Domain.Entities.Enum.Rest;
using SttTools.Domain.Entities.Models.Rest;
using SttTools.Domain.Entities.Models.SpecialModel;
using SttTools.UI.Extensions;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Rest
{

    public class HttpStackModule : IHttpService
    {
        private HttpClient httpClientLoader;
        private Dictionary<TypeApi, UriData> uris;
        private PriorityQueue<Priority, Tuple<int, Task<WebResponse>>> queueRequest;
        private HashSet<Tuple<int, HttpWebResponse>> setResults;
        private readonly string tag = "HttpService";

        public HttpStackModule()
        {
            httpClientLoader = new HttpClient();
            queueRequest = new PriorityQueue<Priority, Tuple<int, Task<WebResponse>>>();
            setResults = new HashSet<Tuple<int, HttpWebResponse>>();
            uris = new Dictionary<TypeApi, UriData>();

            Task.Run(DelayRequests);
        }

        public void InsertUri(TypeApi type, UriData data)
        {
            if (uris.ContainsKey(type))
            {
                uris[type] = data;
            }
            else
            {
                uris.Add(type, data);
            }
        }
        public void UpdateToken(TypeApi type, string tokenType, string accesToken)
        {
            if (!uris.ContainsKey(type))
                throw new ArgumentNullException(type.ToString());

            uris[type].AccesToken = accesToken;
            uris[type].TypeToken = tokenType;
        }

        public Task<ApiResultModel<TResult>> GetRequest<TResult>(string controller, string method,
            Dictionary<string, string> data = null, Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null)
        {
            return Task.Run(() =>
            {
                ApiResultModel<TResult> resultModel;
                try
                {
                    var uriData = GetUriData(typeApi);
                    var url = new Uri(String.Format("{0}/{1}/{2}?{3}", uriData.BaseUri, controller, method, TextsTools.ConvertToGetData(data)));
                    Debug.WriteLine(url);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    request.Method = ConvertRequestType(RequestType.Get);
                    InsertTimeout(request);
                    InsertToken(request.Headers, uriData);

                    resultModel = GetResult<TResult>(request, priority);
                }
                catch (Exception ex)
                {
                    resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Exception, errorMessage: ex.Message));
                }

                return resultModel;
            });
        }
        public Task<ApiResultModel<TResult>> PostRequest<TResult>(string controller, string method,
            Dictionary<string, string> data = null, Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null)
        {
            return Task.Run(() =>
            {
                ApiResultModel<TResult> resultModel;
                try
                {
                    var uriData = GetUriData(typeApi);
                    var url = new Uri(String.Format("{0}/{1}/{2}", uriData.BaseUri, controller, method));
                    Debug.WriteLine(url);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    InsertTimeout(request);
                    InsertToken(request.Headers, uriData);
                    InserdContentType(TextsTools.ConvertToGetData(data), ContentType.UrlEncoded, RequestType.Post, request);

                    resultModel = GetResult<TResult>(request, priority);
                }
                catch (Exception ex)
                {
                    resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Exception, errorMessage: ex.Message));
                }

                return resultModel;
            });
        }
        public Task<ApiResultModel<TResult>> PostRequest<TResult, TRequest>(string controller, string method,
            TRequest obj = default(TRequest), Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null)
        {
            return Task.Run(() =>
            {
                ApiResultModel<TResult> resultModel;
                try
                {
                    var uriData = GetUriData(typeApi);
                    var url = new Uri(String.Format("{0}/{1}/{2}", uriData.BaseUri, controller, method));
                    Debug.WriteLine(url);
                    HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);
                    InsertTimeout(request);
                    InsertToken(request.Headers, uriData);
                    InserdContentType(JsonConvert.SerializeObject(obj), ContentType.Json, RequestType.Post, request);

                    resultModel = GetResult<TResult>(request, priority);
                }
                catch (Exception ex)
                {
                    resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Exception, errorMessage: ex.Message));
                }

                return resultModel;
            });
        }
        public Task<ApiResultModel<TResult>> PutRequest<TResult>(string controller, string method, byte[] data,
            Priority priority = Priority.Low, TypeApi typeApi = TypeApi.DefoltApi, string token = null)
        {
            return Task.Run(() =>
            {
                ApiResultModel<TResult> resultModel;
                try
                {
                    var url = uris[typeApi];
                    var uri = new Uri(string.Format("{0}/{1}/{2}", url.BaseUri, controller, method));
                    Debug.WriteLine("--> " + uri.ToString());
                    var fileContent = new ByteArrayContent(data);
                    fileContent.Headers.ContentType = MediaTypeHeaderValue.Parse("image/png");
                    var multipartContent = new MultipartFormDataContent(GeneratrBoundary());
                    multipartContent.Add(fileContent, "bilddatei", "upload");
                    var response = httpClientLoader.PutAsync(uri, multipartContent).Result;

                    if (response.StatusCode != HttpStatusCode.OK)
                    {
                        resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Default, response.StatusCode));
                    }
                    else
                    {
                        var jsonResult = response.Content.ReadAsStringAsync().Result;
                        var jsonObject = JsonConvert.DeserializeObject<TResult>(jsonResult);
                        resultModel = new ApiResultModel<TResult>(true, jsonObject);
                    }
                }
                catch (Exception ex)
                {
                    return new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Exception, errorMessage: ex.Message));
                }

                return resultModel;
            });
        }
        public Task<byte[]> DownloadObject(string uri, Action<long, long> actionCallbackProgress = null)
        {
            try
            {
                WebClient client = new WebClient();
                client.DownloadProgressChanged += (o, e) => actionCallbackProgress?.Invoke(e.BytesReceived, e.TotalBytesToReceive);
                return client.DownloadDataTaskAsync(new Uri(uri));
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        private ApiResultModel<TResult> GetResult<TResult>(HttpWebRequest request, Priority priority)
        {
            var id = request.GetHashCode();
            queueRequest.Enqueue(priority, new Tuple<int, Task<WebResponse>>(id, new Task<WebResponse>(request.GetResponse)));

            var response = DelayResponse(id);
            ApiResultModel<TResult> resultModel;

            if (response == null)
            {
                resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Timeout, errorMessage: "Timeout"));
                return resultModel;
            }
            var statusCode = response.StatusCode;

            if (statusCode == HttpStatusCode.OK || statusCode == HttpStatusCode.BadRequest)
            {
                try
                {
                    var jsonResult = new StreamReader(response.GetResponseStream()).ReadToEnd();
                    if (jsonResult == null)
                    {
                        resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.JsonIsNullOrEmpty));
                    }
                    else if (statusCode == HttpStatusCode.OK)
                    {
                        var jsonObject = JsonConvert.DeserializeObject<TResult>(jsonResult);
                        resultModel = new ApiResultModel<TResult>(true, jsonObject);
                    }
                    else
                    {
                        var jsonObject = JsonConvert.DeserializeObject<ServiceResult>(jsonResult);
                        resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Default, HttpStatusCode.BadRequest, jsonObject.Error.Description));
                    }
                }
                catch (JsonException ex)
                {
                    resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.ErrorDeserialize));
                }
            }
            else
            {
                resultModel = new ApiResultModel<TResult>(false, errorResult: new ErrorModel(TypeError.Default, statusCode));
            }
            response.Close();
            return resultModel;
        }

        private HttpWebResponse DelayResponse(int id)
        {
            int maxDelaySeconds = 7, delay = 100;
            for (int i = 0; i < maxDelaySeconds * 1000 / delay; ++i)
            {
                if (setResults.Any(_i => _i.Item1 == id))
                {
                    return setResults.First(_i => _i.Item1 == id).Item2;
                }
                else Task.Delay(delay).Wait();
            }

            return null;
        }
        private Task DelayRequests()
        {
            while (true)
            {
                if (queueRequest.Count > 0)
                {
                    var item = queueRequest.Dequeue();
                    try
                    {
                        item.Item2.RunSynchronously();
                        item.Item2.Wait();
                        setResults.Add(new Tuple<int, HttpWebResponse>(item.Item1, (HttpWebResponse)item.Item2.Result));
                    }
                    catch (AggregateException ex)
                    {
                        setResults.Add(new Tuple<int, HttpWebResponse>(item.Item1, null));
                    }
                }
                else
                    Task.Delay(100).Wait();
            }
        }

        private UriData GetUriData(TypeApi typeApi)
        {
            UriData uriData = null;
            if (!uris.TryGetValue(typeApi, out uriData))
            {
                throw new ArgumentException("Current type uri have not uri data");
            }
            return uriData;
        }
        private void InserdContentType(string data, ContentType contentType, RequestType requestType, HttpWebRequest request)
        {
            var byteArray = Encoding.UTF8.GetBytes(data);
            request.ContentType = ConvertContentType(ContentType.UrlEncoded);
            request.ContentLength = byteArray.Length;
            request.Method = ConvertRequestType(requestType);

            Stream dataStream = request.GetRequestStream();
            dataStream.Write(byteArray, 0, byteArray.Length);
            dataStream.Close();
        }
        private void InsertTimeout(HttpWebRequest request)
        {
            request.Timeout = 7000;
        }
        private void InsertToken(WebHeaderCollection webHeaderCollection, UriData uriData)
        {
            if (uriData?.AccesToken == null)
                return;

            webHeaderCollection.Add(HttpRequestHeader.Authorization, uriData.TypeToken + " " + uriData.AccesToken);
        }
        private string GeneratrBoundary()
        {
            return "----------------------------" + DateTime.Now.Ticks.ToString("x");
        }

        private string ConvertContentType(ContentType contentType)
        {
            string result = String.Empty;
            switch (contentType)
            {
                case ContentType.Json:
                    result = "application/json";
                    break;
                case ContentType.UrlEncoded:
                    result = "application/x-www-form-urlencoded";
                    break;
                case ContentType.PngImage:
                    break;
            }

            return result + "; charset=utf-8";
        }
        private string ConvertRequestType(RequestType requestType)
        {
            return requestType.ToString().ToUpper();
        }
    }
}