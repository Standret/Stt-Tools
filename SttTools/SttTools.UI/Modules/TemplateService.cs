﻿using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using Microsoft.Practices.ServiceLocation;
using SttTools.UI.Extensions;

namespace SttTools.UI.Modules
{
    public class TemplateService : ITemplateService
    {
        private readonly IProjectService _projectService;

        private List<TemplatesCreateObjectsViewModel> templates;
        public List<TemplatesCreateObjectsViewModel> Templates
        {
            get
            {
                if (templates == null)
                {
                    templates = _projectService.GloablSettings?.Templates?.TemplateProjects?.Convert()
                        ?? new List<TemplatesCreateObjectsViewModel>();
                }
                return templates;
            }
        }

        public TemplateService()
        {
            _projectService = ServiceLocator.Current.GetInstance<IProjectService>();
        }

        public TemplateFiles FetTemplateFiles(int id)
        {
            throw new NotImplementedException();
        }

        public TemplateProject GetTemplateProject(int id)
        {
            if (id == -1)
                return new TemplateProject();
            var result = _projectService.GloablSettings.Templates.TemplateProjects.Values.
                FirstOrDefault(i => i.Contains(i.FirstOrDefault(it => it.Id == id))).
                FirstOrDefault(i => i.Id == id);

            return result;
        }
    }
}
