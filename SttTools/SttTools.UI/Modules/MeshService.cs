﻿using Microsoft.Practices.ServiceLocation;
using Newtonsoft.Json;
using SttTools.APIModule;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.ResponseModel;
using SttTools.UI.Extensions;
using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.ObjectModel;
using System.Diagnostics;
using System.Linq;
using System.Text;

namespace SttTools.UI.Modules
{
    public class MeshService : IMeshService
    {
        private MeshStructure sourceStructure;

        private readonly IWindowsManagerService _windowsManagerService = ServiceLocator.Current.GetInstance<IWindowsManagerService>();

        public ObservableCollection<MeshNode> LoadDocument(string path)
        {
            var res = new ObservableCollection<MeshNode>();
            try
            {
                var jsonResult = HelperTools.ConvertToString(ECModule.LoadMesh(new StringBuilder(path), 1, 2, 2));
                sourceStructure = JsonConvert.DeserializeObject<MeshStructure>(jsonResult);
                res.Add(sourceStructure.Convert());
                return res;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("--> " + ex.Message);
            }

            return null;
        }

        public ObservableCollection<MeshNode> ReloadDocument()
        {
            var res = new ObservableCollection<MeshNode>();
            try
            {
                var jsonResult = HelperTools.ConvertToString(ECModule.ReloadData());
                sourceStructure = JsonConvert.DeserializeObject<MeshStructure>(jsonResult);
                res.Add(sourceStructure.Convert());
                return res;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("--> " + ex.Message);
            }

            return null;
        }
        public bool Save()
        {
            try
            {
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("--> " + ex.Message);
                return false;
            }
        }
        public ObservableCollection<MeshInfo> GetMeshInfo()
        {
            var infos = new ObservableCollection<MeshInfo>();
            MeshStructure structure = null;
            for (int index = 0; true; ++index)
            {
                positionCurrentElement = 0;
                structure = GetStructure("material", sourceStructure, index);
                if (structure == null)
                    break;

                infos.Add(new MeshInfo(GetValueString("diff", structure), 
                    GetValueString("n", structure), 
                    GetValueString("spec", structure), 
                    GetValueString("shader", structure)));
            }

            return infos;
        }

        private int positionCurrentElement = 0;
        private MeshStructure GetStructure(string key, MeshStructure parent, int positioonSearchElement = 0)
        {
            foreach (var item in parent.Sections)
            {
                if (item.Name == key)
                {
                    if (positionCurrentElement == positioonSearchElement)
                        return item;
                    ++positionCurrentElement;
                }
                else
                {
                    var res = GetStructure(key, item, positioonSearchElement);
                    if (res != null)
                        return res;
                }
            }

            return null;
        }

        private string GetValueString(string key)
        {
            return GetValueString(key, sourceStructure);
        }

        private string GetValueString(string key, MeshStructure structure)
        {
            ResponseElement<string>[] stringValue;
            if (structure.ElementsString.TryGetValue(key, out stringValue))
                return stringValue.First().Value.First(); // todo: problem

            foreach (var item in structure.Sections)
            {
                var res = GetValueString(key, item);
                if (res != null)
                    return res;
            }

            return null;
        }

        private string GetDiffuseTextureName()
        {
            return GetValueString("diff");
        }
        private string GetNormalTextureName()
        {
            return GetValueString("n");
        }
        private string GetSpecularTextureName()
        {
            return GetValueString("spec");
        }
        private string GetShaderName()
        {
            return GetValueString("shader");
        }

        public string GetValues(TypeSection type, int id, int count)
        {
            string result = String.Empty;
            switch (type)
            {
                case TypeSection.Int:
                    {
                        var result_arr = HelperTools.ConvertToArrayInt(ECModule.GetValuesInt((uint)id, (uint)count));
                        if (result_arr == null)
                            break;

                        result = String.Join(", ", result_arr);
                        break;
                    }
                case TypeSection.Float:
                    {
                        var result_arr = HelperTools.ConvertToArrayFloat(ECModule.GetValuesFloat((uint)id, (uint)count));
                        if (result_arr == null)
                            break;

                        result = String.Join(", ", result_arr);
                    break;
                    }
            }

            return result;
        }

        public bool Save(bool saveMode1, bool saveMode2)
        {
            try
            {
                int mode = 0;
                if (saveMode1)
                    mode = 1;
                else if (saveMode2)
                    mode = 2;

                return ECModule.Save(mode);
            }
            catch (Exception ex)
            {
                _windowsManagerService.ShowMessageBox("external exception in c++, acces denied", TypeMessage.Error);
                return false;
            }
        }

        public bool ChangeNodeName(string newName, int id)
        {
            return ECModule.ChangeNodeName(new StringBuilder(newName), (uint)id);
        }
    }
}
