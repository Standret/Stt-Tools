﻿using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.UI.Modules.Abstract;
using System;
using System.Windows;

namespace SttTools.UI.Modules
{
    public class WindowsManagerService : IWindowsManagerService
    {
        public LocationPath OpenDialogFileBrowser()
        {
            var dialog = new System.Windows.Forms.OpenFileDialog();
            dialog.Filter = "EconomicCrisis (*.ecproj)|*.ecproj";
            dialog.DefaultExt = ".ecproj";
            dialog.ShowDialog();
            if (dialog.FileName == null)
                return null;

            var res = new LocationPath
            {
                ExtensionFile = "ecproj",
            };

            if (!String.IsNullOrWhiteSpace(dialog.SafeFileName))
            {
                res.NameFile = dialog.SafeFileName.Remove(dialog.SafeFileName.LastIndexOf(".ecproj"));
                res.BasePath = dialog.FileName.Remove(dialog.FileName.LastIndexOf(dialog.SafeFileName));
            }

            return res;
        }

        public string OpenDialogFolderBrowser()
        {
            var dialog = new System.Windows.Forms.FolderBrowserDialog();
            System.Windows.Forms.DialogResult result = dialog.ShowDialog();
            return dialog.SelectedPath;
        }

        public void ShowMessageBox(string message, TypeMessage type)
        {
            MessageBox.Show(message, type.ToString());
        }
    }
}
