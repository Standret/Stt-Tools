﻿using SttTools.UI.Modules.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SttTools.Domain.Entities.Models;
using Microsoft.Practices.ServiceLocation;
using SttTools.UI.Model;
using SttTools.Domain.Entities.Models.SettingsPrograms;
using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.Enum.SettingsPrograms;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using SttTools.UI.Extensions;

namespace SttTools.UI.Modules
{
    public class ProjectService : IProjectService
    {
        private readonly IKernelService kernelService;
        private string currentSettingsFile = AppDomain.CurrentDomain.BaseDirectory + "\\" + Constants.SettingsFileName;

        private List<TemplatesCreateObjectsViewModel> templates;
        public List<TemplatesCreateObjectsViewModel> Templates
        {
            get
            { 
                if (GloablSettings?.Templates == null)
                    return new List<TemplatesCreateObjectsViewModel>();
                return GloablSettings.Templates.TemplateProjects.Convert();
            }
        }

        public GeneralProgramSettings GloablSettings
        {
            get
            {
                if (MemoryCache.Settings == null)
                    MemoryCache.Settings = LoadGeneralSettings().Result;

                return MemoryCache.Settings;
            }
        }

        public ProjectService()
        {
            kernelService = ServiceLocator.Current.GetInstance<IKernelService>();
            Task.Run(async () => MemoryCache.Settings = await LoadGeneralSettings());
        }
        
        public async Task<IEnumerable<ProjectSettings>> GetSettingProject()
        {
            if (MemoryCache.Settings == null)
            {
                var tempSettings = await kernelService.Load<GeneralProgramSettings>(currentSettingsFile);
                MemoryCache.Settings = tempSettings ?? new GeneralProgramSettings();
            }

            return MemoryCache.Settings?.Projects;
        }
        public void PickProject(int id)
        {
            throw new NotImplementedException();
        }
        public void RegisterProject(string fullPath)
        {
            if (MemoryCache.Settings.Projects == null)
                MemoryCache.Settings.Projects = new List<ProjectSettings>();
            MemoryCache.Settings.Projects.Add(new ProjectSettings(fullPath));
            Task.Run(UpdateFile);
        }
        public void RemoveProject(int id)
        {
            throw new NotImplementedException();
        }

        private async Task UpdateFile()
        {
            await kernelService.Save(currentSettingsFile, MemoryCache.Settings);
        }
        private GeneralProgramSettings DefaultSettings()
        {
            var result = new GeneralProgramSettings
            {
                Projects = new List<ProjectSettings>(),
                Templates = new BaseTemplate
                {
                    TemplateProjects = new Dictionary<string, List<TemplateProject>>()
                }
            };

            result.Templates.TemplateProjects.Add("Hearts of Iron IV", new List<TemplateProject>());
            result.Templates.TemplateProjects.Add("SMV", new List<TemplateProject>());
            result.Templates.TemplateProjects["SMV"].Add(new TemplateProject
            {
                Name = "Empty project",
                Decription = "Lorem ipsum bla bla",
                DefaultProjectName = "Simulation",
                ExtensionsProjectFile = "smvproj",
                CreateDirectoryForProject = true,
                CreateBaseArchitecture = false,
                TemplateFiles = new Dictionary<string, List<TemplateFiles>>()
            });
            result.Templates.TemplateProjects["Hearts of Iron IV"].Add(new TemplateProject
            {
                Name = "Existing project",
                Decription = "This project for existing modification Economic Crisis",
                DefaultProjectName = "EconomicCrisisProject",
                ExtensionsProjectFile = "ecproj",
                CreateDirectoryForProject = false,
                CreateBaseArchitecture = false,
                TemplateFiles = new Dictionary<string, List<TemplateFiles>>()
            });
            result.Templates.TemplateProjects["Hearts of Iron IV"].Add(new TemplateProject
            {
                Name = "Empty project",
                Decription = "Empty project for Economic Crisis modification",
                DefaultProjectName = "EconomicCrisisProject",
                ExtensionsProjectFile = "ecproj",
                CreateDirectoryForProject = false,
                CreateBaseArchitecture = false,
                TemplateFiles = new Dictionary<string, List<TemplateFiles>>()
            });
            result.Templates.TemplateProjects["Hearts of Iron IV"].Last().TemplateFiles.Add("General", new List<TemplateFiles>
            {
                new TemplateFiles
                {
                    Name = "Asset",
                    Decription = "assets file",
                    ExtensionsProjectFile = "asset",
                    FileType = FileType.Asset,
                },
                new TemplateFiles
                {
                    Name = "Mesh",
                    Decription = "mesh 3d file",
                    ExtensionsProjectFile = "mesh",
                    FileType = FileType.Mesh,
                },
                new TemplateFiles
                {
                    Name = "Animation",
                    Decription = "animations file",
                    ExtensionsProjectFile = "anim",
                    FileType = FileType.Anim,
                }
            });

            return result;
        }
        private async Task<GeneralProgramSettings> LoadGeneralSettings()
        {
            var resultLoad = await kernelService.Load<GeneralProgramSettings>(currentSettingsFile);
            if (resultLoad == null)
            {
                resultLoad = DefaultSettings();
                await kernelService.Save(currentSettingsFile, resultLoad, true);
            }
            InsertId(resultLoad);
            return resultLoad;
        }

        private void InsertId(GeneralProgramSettings settings)
        {
            int oldId = 0;

            foreach (var templatesProjects in settings.Templates.TemplateProjects.Values)
            {
                foreach (var templateProject in templatesProjects)
                {
                    templateProject.Id = oldId++;

                    foreach (var templatesFiles in templateProject.TemplateFiles.Values)
                    {
                        foreach (var templateFile in templatesFiles)
                        {
                            templateFile.Id = oldId++;
                        }
                    }
                }
            }
        }
    }
}