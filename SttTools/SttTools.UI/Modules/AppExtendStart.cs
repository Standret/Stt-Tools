﻿using Microsoft.Practices.ServiceLocation;
using Prism.Modularity;
using SttTools.Domain.Entities.Enum;
using SttTools.UI.Modules.Abstract;
using SttTools.UI.View;
using SttTools.UI.View.Menu;
using SttTools.UI.ViewModel;
using SttTools.UI.ViewModel.Menu;

namespace SttTools.UI.Modules
{
    public class AppExtendStart : IModule
    {
        public void Initialize()
        {
            var navigationModule = ServiceLocator.Current.GetInstance<INavigationModule>();
            if (MemoryCache.CommandLineArgs?.Length > 0)
                navigationModule.Navigate<MeshDataStructureViewModel, MeshDataStructureView>(Regions.Main);
            else
                navigationModule.Navigate<StartViewModel, StartView>(Regions.Main);

            navigationModule.Navigate<TopMenuViewModel, TopMenuView>(Regions.Menu);
        }
    }
}
