﻿using Prism.Modularity;
using Prism.Regions;
using SttTools.Domain.Entities.Enum;
using SttTools.UI.Model.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Controls;

namespace SttTools.UI.Modules.Abstract
{
    public interface INavigationModule : IModule
    {
        void Navigate<TViewModel, TView>(Regions region = Regions.Main)
            where TViewModel : class
            where TView : UserControl;
        void Navigate<TViewModel, TView, TParametr>(TParametr parametr, Regions region = Regions.Main)
             where TViewModel : class
             where TView : UserControl
             where TParametr : class;
    }
}
