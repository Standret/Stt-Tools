﻿using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface IWindowsManagerService
    {
        string OpenDialogFolderBrowser();
        LocationPath OpenDialogFileBrowser();
        void ShowMessageBox(string message, TypeMessage type);
    }
}
