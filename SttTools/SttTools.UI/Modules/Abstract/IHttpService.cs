﻿using SttTools.Domain.Entities.Enum.Rest;
using SttTools.Domain.Entities.Models.Rest;
using SttTools.UI.Modules.Rest;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface IHttpService
    {
        Task<ApiResultModel<TResult>> GetRequest<TResult>(string controller, string method,
            Dictionary<string, string> data = null, Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null);
        Task<byte[]> DownloadObject(string uri, Action<long, long> actionCallbackProgress = null);
        Task<ApiResultModel<TResult>> PostRequest<TResult, TRequest>(string controller,
            string method, TRequest obj = default(TRequest), Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null);
        Task<ApiResultModel<TResult>> PostRequest<TResult>(string controller, string method,
            Dictionary<string, string> data = null, Priority priority = Priority.Medium,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null);
        Task<ApiResultModel<TResult>> PutRequest<TResult>(string controller, string method,
            byte[] data, Priority priority = Priority.Low,
            TypeApi typeApi = TypeApi.DefoltApi, string token = null);
        void InsertUri(TypeApi type, UriData data);
        void UpdateToken(TypeApi type, string tokenType, string accesToken);
    }
}
