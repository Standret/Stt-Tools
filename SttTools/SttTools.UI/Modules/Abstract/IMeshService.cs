﻿using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using System.Collections.ObjectModel;

namespace SttTools.UI.Modules.Abstract
{
    public interface IMeshService
    {
        ObservableCollection<MeshNode> LoadDocument(string path);
        ObservableCollection<MeshNode> ReloadDocument();
        ObservableCollection<MeshInfo> GetMeshInfo();
        string GetValues(TypeSection type, int id, int count);
        bool ChangeNodeName(string newName, int id);
        bool Save(bool saveMode1, bool saveMode2);
    }
}
