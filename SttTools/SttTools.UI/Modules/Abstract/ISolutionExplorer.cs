﻿using SttTools.Domain.Entities.Models;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface ISolutionExplorer
    {
        //event Action<string> LoadSolution;
        Task<ObservableCollection<NodeCatalog>> GetSolutionCatalogs(string fullPath);
        //Task<ObservableCollection<NodeCatalog>> GetSolutionCatalogs(string setting);
        void Load(string path);
        void Load(LocationPath path);
        Task<bool> InitializeSolutionCatalog(LocationPath path);
    }
}
