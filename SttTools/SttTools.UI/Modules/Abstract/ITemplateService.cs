﻿using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface ITemplateService
    {
        List<TemplatesCreateObjectsViewModel> Templates { get; }

        TemplateProject GetTemplateProject(int id);
        TemplateFiles FetTemplateFiles(int id);
    }
}
