﻿using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface IKernelService
    {
        Task<bool> Save(string path, object obj, bool hummanReadable = true);
        Task<T> Load<T>(string path);
    }
}
