﻿using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Threading.Tasks;

namespace SttTools.UI.Modules.Abstract
{
    public interface IProjectService
    {
        Task<IEnumerable<ProjectSettings>> GetSettingProject();

        void RegisterProject(string fullPath);
        void RemoveProject(int id);
        void PickProject(int id);

        GeneralProgramSettings GloablSettings { get; }
    }
}
