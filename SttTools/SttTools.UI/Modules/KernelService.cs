﻿using Newtonsoft.Json;
using SttTools.UI.Modules.Abstract;
using System;
using System.Diagnostics;
using System.IO;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Modules
{
    public class KernelService : IKernelService
    {
        public async Task<bool> Save(string path, object obj, bool hummanReadable = true)
        {
            try
            {
                var fstream = new FileStream(path, FileMode.OpenOrCreate);
                
                byte[] array = Encoding.Default.GetBytes(JsonConvert.SerializeObject(obj, hummanReadable ? Formatting.Indented : Formatting.None));
                await fstream.WriteAsync(array, 0, array.Length);
                fstream.Close();
                return true;
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Kernel error: " + ex.Message);
                return false;
            }
        }

        public async Task<T> Load<T>(string path)
        {
            try
            {
                var stream = File.OpenRead(path);
                byte[] array = new byte[stream.Length];

                await stream.ReadAsync(array, 0, array.Length);
                string result = Encoding.Default.GetString(array);
                return JsonConvert.DeserializeObject<T>(result);
            }
            catch (Exception ex)
            {
                Debug.WriteLine("Kernel error: " + ex.Message);
                return default(T);
            }
        }
    }
}
