[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SttTools.UI.ModaleWindows.CreateProjectWindow.CreateProjectBootstrapper.#CreateProject")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SttTools.UI.ModaleWindows.CreateProjectWindow.CreateProjectWindowViewModel.#CloseDialogWindow")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SttTools.UI.ModaleWindows.CreateProjectWindow.CreateProjectWindowViewModel.#CreateProject")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SttTools.UI.Modules.SolutionExplorer.#LoadSolution")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "SttTools.UI.ViewModel.BaseViewModel.#Prism.Regions.INavigationAware.IsNavigationTarget(Prism.Regions.NavigationContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "SttTools.UI.ViewModel.BaseViewModel.#Prism.Regions.INavigationAware.OnNavigatedFrom(Prism.Regions.NavigationContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "SttTools.UI.ViewModel.BaseViewModel.#Prism.Regions.INavigationAware.OnNavigatedTo(Prism.Regions.NavigationContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1033:InterfaceMethodsShouldBeCallableByChildTypes", Scope = "member", Target = "SttTools.UI.ViewModel.BaseViewModel`1.#Prism.Regions.INavigationAware.OnNavigatedTo(Prism.Regions.NavigationContext)")]
[assembly: System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1009:DeclareEventHandlersCorrectly", Scope = "member", Target = "SttTools.UI.ViewModel.SolutionExplorerViewModel.#Selected")]
// This file is used by Code Analysis to maintain SuppressMessage 
// attributes that are applied to this project.
// Project-level suppressions either have no target or are given 
// a specific target and scoped to a namespace, type, member, etc.
//
// To add a suppression to this file, right-click the message in the 
// Code Analysis results, point to "Suppress Message", and click 
// "In Suppression File".
// You do not need to add suppressions to this file manually.

