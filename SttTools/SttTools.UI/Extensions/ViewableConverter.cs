﻿using System;
using SttTools.Domain.Entities.Enum;
using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using SttTools.Domain.Entities.ResponseModel;
using SttTools.Domain.Entities.ViewModels.CreateProject;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;

namespace SttTools.UI.Extensions
{
    public static class ViewableExtensionsConverter
    {
        public static MeshNode Convert(this MeshStructure meshStructure)
        {
            var result = new MeshNode("root", TypeSection.Section);
            meshStructure.Convert(result.ChildrenGeneral);
            return result;
        }
        public static List<TemplatesCreateObjectsViewModel> Convert(this Dictionary<string, List<TemplateProject>> models)
        {
            var result = new List<TemplatesCreateObjectsViewModel>();
            result.Add(new TemplatesCreateObjectsViewModel("Games"));

            foreach (var item in models)
            {
                result.Last().Children.Add(new TemplatesCreateObjectsViewModel(item.Key, item.Value.Convert()));
                foreach (var itemProject in result.Last().Children.Last().Projects)
                {
                    itemProject.TypeObjects = item.Key;
                }
            }

            return result;
        }
        public static List<TemplateObjects> Convert(this List<TemplateProject> models)
        {
            return models.Select(i => new TemplateObjects(i.Name, i.TypeProject, i.Decription, i.Image, i.Id)).ToList();
        }
        private static void Convert(this MeshStructure meshStructure, ObservableCollection<MeshNode> parent)
        {
            parent.Add(new MeshNode(meshStructure.Name, TypeSection.Section, meshStructure.Id));
            foreach (var section in meshStructure.Sections)
                section.Convert(parent.Last().ChildrenGeneral);

            foreach (var str in meshStructure.ElementsString)
                parent.Last().ChildrenGeneral.Add(new MeshNode(str.Key, TypeSection.String, str.Value.First().Id, str.Value.First().Count, String.Join(", ", str.Value.First().Value.ConvertToString()))); // todo: problem

            foreach (var str in meshStructure.ElementsInt)
                parent.Last().ChildrenGeneral.Add(new MeshNode(str.Key, TypeSection.Int, str.Value.First().Id, str.Value.First().Count, String.Join(", ", str.Value.First().Value)));

            foreach (var str in meshStructure.ElementsFloat)
                parent.Last().ChildrenGeneral.Add(new MeshNode(str.Key, TypeSection.Float, str.Value.First().Id, str.Value.First().Count, String.Join(", ", str.Value.First().Value)));
        }
        private static string ConvertToString(this string[] array)
        {
            StringBuilder text = new StringBuilder();

            foreach (var item in array)
            {
                text.Append("\"");
                text.Append(item);
                text.Append("\", ");
            }

            text.Remove(text.Length - 2, 2);

            return text.ToString();
        }
    }
}
