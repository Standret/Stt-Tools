﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SttTools.UI.Extensions
{
    public static class TextsTools
    {
        public static string ConvertToGetData(Dictionary<string, string> dict, bool decorateName = false)
        {
            StringBuilder text = new StringBuilder();
            if (dict != null)
            {
                foreach (var tempDict in dict)
                {
                    if (decorateName)
                    {
                        text.AppendFormat("{0}={1}&", tempDict.Key, Regex.Replace(tempDict.Value, "@", "%"));
                    }
                    else
                    {
                        text.AppendFormat("{0}={1}&", tempDict.Key, tempDict.Value);
                    }
                }
                text.Remove(text.Length - 1, 1);
            }
            return text.ToString();
        }
    }
}
