﻿using SttTools.Domain.Entities.Models;
using SttTools.Domain.Entities.Models.SettingsPrograms.ProjectModels;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;

namespace SttTools.UI.Extensions
{
    public static class DirectoriseExtensions
    {
        private static List<string> names = new List<string>();
        static DirectoriseExtensions()
        {
            names.Add(@"$recycle.bin");
            names.Add(@".git");
        }
        public static bool IsDeniedFolder(this string name)
        {
            var editName = name.ToLower();
            foreach (var itemName in names)
            {
                if (editName == itemName)
                    return true;
            }
            return false;
        }

        public static ObservableCollection<NodeCatalog> ConvertToNodes(this MainProjectsFiles files, string fullPath)
        {
            var collection = new NodeCatalog("tempory");
            ConvertToNodes(files, collection.Children, fullPath);
            return collection.Children.Last().Children;
        }

        private static void ConvertToNodes(this MainProjectsFiles files, ObservableCollection<NodeCatalog> parent, string fullPath)
        {
            var basePath = fullPath.Substring(0, fullPath.LastIndexOf('\\') + 1) + files.BasePath;
            parent.Add(new NodeCatalog(basePath));

            foreach (var folder in files.Folders)
                ConvertToNodes(folder, parent.Last().Children, fullPath);

            foreach (var file in files.Files)
                parent.Last().Children.Add(new NodeCatalog(basePath + '\\' + file));
        }

        // private static NodeCatalog Convert(this Pair<string, List<string>> model, NodeCatalog parent)
        //  {
        //    parent.
        //  }
    }
}