﻿using Prism.Modularity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Extensions
{
    public static class MainExtensions
    {
        public static void RegisterModule<TModule>(this IModuleCatalog moduleCatalog)
        {
            var type = typeof(TModule);
;            moduleCatalog.AddModule(new ModuleInfo
            {
                ModuleName = type.Name,
                ModuleType = type.AssemblyQualifiedName,
                InitializationMode = InitializationMode.WhenAvailable
            });
        }
    }
}
