﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI.Extensions
{
    public static class EnumerableConverter
    {
        public static ObservableCollection<T> ToObservable<T>(this IEnumerable<T> enumerable)
        {
            ObservableCollection<T> result = new ObservableCollection<T>();
            foreach (var item in enumerable)
                result.Add(item);

            return result;
        }
    }
}
