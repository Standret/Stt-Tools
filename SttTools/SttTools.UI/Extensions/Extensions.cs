﻿namespace SttTools.UI.Extensions
{
    public static class Extensions
    {
        public static bool IsRegisterExtension(string extension)
        {
            return extension == "mesh" || extension == "anim";
        }
    }
}
