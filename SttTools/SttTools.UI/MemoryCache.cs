﻿using SttTools.Domain.Entities.Models.SettingsPrograms;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.UI
{
    public static class MemoryCache
    {
        private static Dictionary<string, object> blob;

        public static string[] CommandLineArgs { get; set; }
        public static GeneralProgramSettings Settings { get; set; }

        static MemoryCache()
        {
            blob = new Dictionary<string, object>();
        }

        public static void Add(string key, object obj)
        {
            blob.Add(key, obj);
        }
        public static void Remove(string key)
        {
            if (blob.ContainsKey(key))
                blob.Remove(key);
        }
        public static void Update(string key, object obj)
        {
            Remove(key);
            Add(key, obj);
        }
    }
}
