﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SttTools.UI.ViewModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SttTools.UI.View
{
    /// <summary>
    /// Interaction logic for StartView.xaml
    /// </summary>
    public partial class StartView : UserControl, IBaseView<StartViewModel>
    {
        public StartView()
        {
            InitializeComponent();
            DataContext = ViewModel = ServiceLocator.Current.GetInstance<IUnityContainer>().Resolve<StartViewModel>();
        }

        public StartViewModel ViewModel { get; private set; }

        private void ListBox_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            var dataContextOS = ((FrameworkElement)e.OriginalSource).DataContext;
            var dataContextS = ((FrameworkElement)e.Source).DataContext;

            if (dataContextOS != dataContextS)
            {
                ViewModel.SelectAndLoad();
            }
        }
    }
}
