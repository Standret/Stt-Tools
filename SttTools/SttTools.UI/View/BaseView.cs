﻿using Microsoft.Practices.ServiceLocation;
using Microsoft.Practices.Unity;
using SttTools.UI.ViewModel;
using System.Windows.Controls;

namespace SttTools.UI.View
{
    public interface IBaseView<TViewModel>
        where TViewModel : BaseViewModel
    {
        TViewModel ViewModel { get; }
    }
}
