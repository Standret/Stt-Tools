﻿using SttTools.Domain.Entities.ViewModels.CreateProject;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SttTools.UI.TemplateView
{
    /// <summary>
    /// Interaction logic for SomeCreatTemplate.xaml
    /// </summary>
    public partial class SomeCreateTemplate
    {
        public SomeCreateTemplate()
        {
            InitializeComponent();
        }

#region DependecyProjects
        #region TemplatesData
        public List<TemplatesCreateObjectsViewModel> TemplatesData
        {
            get { return (List<TemplatesCreateObjectsViewModel>)GetValue(TemplatesDataProperty); }
            set { SetValue(TemplatesDataProperty, value); }
        }

        public static readonly DependencyProperty TemplatesDataProperty =
            DependencyProperty.Register("TemplatesData", typeof(List<TemplatesCreateObjectsViewModel>),
                typeof(SomeCreateTemplate), new PropertyMetadata(OnChangeTemplates));

        private static void OnChangeTemplates(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
        {
            var self = (SomeCreateTemplate)depObj;
            self._treeView.ItemsSource = args.NewValue as List<TemplatesCreateObjectsViewModel>;
        }
        #endregion

        #region SelectedProject
        public TemplateObjects SelectedTemplate
        {
            get { return (TemplateObjects)GetValue(SelectedTemplateProperty); }
            set { SetValue(SelectedTemplateProperty, value); }
        }

        public static readonly DependencyProperty SelectedTemplateProperty =
            DependencyProperty.Register("SelectedTemplate", typeof(TemplateObjects), typeof(SomeCreateTemplate),
                new FrameworkPropertyMetadata((o, i) => Debug.WriteLine("-->Test"))
                {
                    BindsTwoWayByDefault = true, DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
                });

        #endregion
#endregion

        private void TreeView_SelectedItemChanged(object sender, RoutedPropertyChangedEventArgs<object> e)
        {
            _listBox.ItemsSource = ((TemplatesCreateObjectsViewModel)e.NewValue).Projects;
            _listBox.SelectedIndex = -1;
            _listBox.SelectionChanged += _listBox_SelectionChanged;
        }

        private void _listBox_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selected = (TemplateObjects)_listBox.SelectedItem;
            _lblType.Text = selected?.TypeObjects;
            _lblDescription.Text = selected?.Description;
            SelectedTemplate = selected;
        }
    }
}