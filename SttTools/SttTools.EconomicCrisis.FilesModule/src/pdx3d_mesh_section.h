#pragma once
#include "pdx3d_mesh_element.h"
#include <map>
#include <list>
#include <array>
#include <cstring>
#include <json.hpp>

namespace pdx_document
{
	namespace graph
	{
		using json = nlohmann::json;

		class mesh_section
		{
		public:
			mesh_section() = delete;
			mesh_section(const std::string &name, unsigned id) : name(name), id(id) { }
			void set_element(int *value, int count, std::string &name);
			void set_element(float *value, int count, std::string &name);
			void set_element(std::string *value, int count, std::string &name);
			void set_section(const std::string &value, unsigned id);
			json get_json(int max_return_values_str, int max_return_values_float, int max_return_values_int) const;
			inline std::string get_name() const { return name; }
			mesh_section &get_section(unsigned id);
			const int *get_values_int(unsigned id) const;
			const float *get_values_float(unsigned id) const;
			inline unsigned get_id() { return id; }
			inline void set_name(const std::string &name) { this->name = name; }
			inline std::multimap<std::string, mesh_element<int>> *get_elements_int() { return &elements_int; }
			inline std::multimap<std::string, mesh_element<float>> *get_elements_float() { return &elements_float; }
			inline std::multimap<std::string, mesh_element<std::string>> *get_elements_string() { return &elements_char; }
			inline std::list<mesh_section> *get_sections() { return &sections; }
		private:
			std::multimap<std::string, mesh_element<int>> elements_int;
			std::multimap<std::string, mesh_element<float>> elements_float;
			std::multimap<std::string, mesh_element<std::string>> elements_char;
			std::list<mesh_section> sections;
			std::string name;
			unsigned id;

			void convert_to_json(nlohmann::basic_json<> &base, int max_return_values_str, int max_return_values_float, int max_return_values_int) const;
			const int *get_values_int(const mesh_section &parent, unsigned id) const;
			const float *get_values_float(const mesh_section &parent, unsigned id) const;
		};

		unsigned get_uniqueId();
		template<class T>
		std::vector<T> convert_to_vector(const T *ptr, size_t count);
		template<class T>
		const T *get_value(const std::multimap<std::string, mesh_element<T>> *models, unsigned id);
		int correct_count(int count, int max);
	}
}