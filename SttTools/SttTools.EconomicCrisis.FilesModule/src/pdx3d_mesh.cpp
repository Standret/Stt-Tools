#include "pdx3d_mesh.h"

namespace pdx_document
{
	namespace graph
	{
		mesh::mesh(const std::string &name) : mesh(fs::path(name)) { }
		mesh::mesh(const char *name) : mesh(std::string(name)) {}
		mesh::mesh(const fs::path &path) : data_structure(mesh_section("data_structure", get_uniqueId()))
		{
			if (!fs::exists(path))
				throw TRACE_EXCEPTION("Invalid path");
			if (fs::is_directory(path))
				throw TRACE_EXCEPTION("path is directory");
			auto extension = path.extension().string();
			if (extension != ".mesh" && extension != ".anim")
				throw TRACE_EXCEPTION("invalid extension");
			full_path = path;
			file.open(path, std::ios::binary | std::ios::in);
		}

		bool mesh::load()
		{
			is_changed = true;
			if (!is_supported_version())
				throw TRACE_EXCEPTION("unsopported version mesh");
			load(data_structure, 0);
			file.close();
		}
		bool mesh::save(int mode)
		{
			// 0 - save as copy with postfix _temp
			// 1 - save as new orginal, but orginal save as postfix _orgtemp
			// 2 - save as new oginal, and orginal file delete

			auto str = full_path.parent_path().string() + "\\" + full_path.filename().string();
			auto extension = full_path.extension().string();
			str = str.substr(0, str.length() - extension.length());
			switch (mode) {
			case 0:
				str += "_EC_temp";
				break;
			case 1:
				fs::rename(full_path, fs::path(str + "_EC_orgtemp" + extension));
				break;
			case 2:
				fs::remove(full_path);
				break;
			default:
				return false;
			}
			
			str += extension;
			file.open(fs::path(str), std::ios::binary | std::ios::out);
			if (!file.is_open())
				return false;

			insert_general_id();
			save(data_structure);
			return true;
		}
		const char *mesh::get_json_string(int max_return_values_str, int max_return_values_float, int max_return_values_int) const
		{
			if (is_changed) {
				auto json_res = data_structure.get_json(max_return_values_str, max_return_values_float, max_return_values_int);
				json_result = json_res.dump();
			}
			return json_result.c_str();
		}
		const int *mesh::get_value_int(unsigned id) const
		{
			return data_structure.get_values_int(id);
		}
		const float *mesh::get_value_float(unsigned id) const
		{
			return data_structure.get_values_float(id);
		}

		bool mesh::change_node_name(const char *name, unsigned id)
		{
			is_changed = true;
			return change_node_name(data_structure, name, id);
		}

		bool mesh::is_supported_version()
		{
			char *temp = new char[general_id.length()];
			file.read(temp, general_id.length());
			if (strncmp(general_id.c_str(), temp, general_id.length())) {
				delete[] temp;
				return false;
			}
			delete[] temp;
			return true;
		}
		void mesh::load(mesh_section & parent, int currentLevel)
		{
			auto type_section = get_type_section();
			while (!file.eof()) {
				switch (type_section) {
				case Resources::TypeMeshSection::Element:
					load_element(parent);
					break;
				case Resources::TypeMeshSection::Section:
					int level = get_level_section();
					if (currentLevel >= level) {
						for (int i = 0; i < level; i++)
							file.unget();
						return;
					}
					else {
						auto name_section = get_name_section();
						unsigned id = get_uniqueId();
						parent.set_section(name_section, id);
						load(parent.get_section(id), currentLevel + 1);
						break;
					}
					break;
				}
				type_section = get_type_section();
			}
		}
		void mesh::save(mesh_section &parent, int current_level) const
		{
			if (current_level != 0) {
				set_type_section(TypeMeshSection::Section, current_level);
				file.write(parent.get_name().c_str(), parent.get_name().length());
				file.put('\0');
			}
			file.flush();
			for (auto &item_float : *parent.get_elements_float()) {
				set_type_section(TypeMeshSection::Element);
				insert_name_node(item_float.second.GetName());
				set_type_element(TypeMeshElement::Float);
				set_size_array(item_float.second.GetCount());
				file.write((char*)item_float.second.GetValue(), 4 * item_float.second.GetCount());
				file.flush();
			}
			for (const auto &item_int : *parent.get_elements_int()) {
				set_type_section(TypeMeshSection::Element);
				insert_name_node(item_int.second.GetName());
				set_type_element(TypeMeshElement::Int);
				set_size_array(item_int.second.GetCount());
				file.write((char*)item_int.second.GetValue(), 4 * item_int.second.GetCount());
				file.flush();
			}
			for (const auto &item_string : *parent.get_elements_string()) {
				set_type_section(TypeMeshSection::Element);
				insert_name_node(item_string.second.GetName());
				set_type_element(TypeMeshElement::String);
				set_size_array(item_string.second.GetCount());

				auto str_value = item_string.second.GetValue();
				for (int i = 0; i < item_string.second.GetCount(); ++i) {
					set_size_array(str_value[i].length() + 1);
					file.write(str_value[i].c_str(), str_value[i].length());
					file.put('\0');
				}
				file.flush();
			}
			for (mesh_section &section : *parent.get_sections())
				save(section, current_level + 1);
		}
		void mesh::load_element(mesh_section &section)
		{
			auto name_element = get_name_element();
			auto type = get_type_element();
			int size_array = get_int_sizes();
			switch (type) {
			case Resources::TypeMeshElement::Int: {
				auto arr = get_int_elements(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			case Resources::TypeMeshElement::Float: {
				auto arr = get_float_elements(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			case Resources::TypeMeshElement::String: {
				auto arr = get_value_strings(size_array);
				section.set_element(arr, size_array, name_element);
				return;
			}
			}
		}

		void mesh::insert_general_id()
		{
			file.write(general_id.c_str(), general_id.length());
		}
		void mesh::insert_name_node(const std::string &name) const
		{
			char size = name.length();
			file.put(size);
			file.write(name.c_str(), name.length());
		}

		TypeMeshSection mesh::get_type_section()
		{
			char x;
			file.read(&x, 1);
			if (file.eof())
				return TypeMeshSection::None;
			switch (x) {
			case '!':
				return TypeMeshSection::Element;
				break;
			case '[':
				file.unget();
				return TypeMeshSection::Section;
				break;
			default:
				throw TRACE_EXCEPTION("incorrect parse meshFile: " + get_name_file());
			}
		}
		TypeMeshElement mesh::get_type_element()
		{
			char x;
			file.read(&x, 1);
			switch (x) {
			case 'i':
				return TypeMeshElement::Int;
				break;
			case 'f':
				return TypeMeshElement::Float;
				break;
			case 's':
				return TypeMeshElement::String;
				break;
			default:
				throw TRACE_EXCEPTION("incorrect parse meshFile: " + get_name_file());
			}
		}
		void mesh::set_type_section(TypeMeshSection type, int level) const
		{
			switch (type) {
			case TypeMeshSection::Element:
				file.put('!');
				break;
			case TypeMeshSection::Section:
				for (int i = 0; i < level; ++i)
					file.put('[');
				break;
			}
		}
		void mesh::set_type_element(TypeMeshElement type) const
		{
			switch (type) {
			case TypeMeshElement::Float:
				file.put('f');
				break;
			case TypeMeshElement::Int:
				file.put('i');
				break;
			case TypeMeshElement::String:
				file.put('s');
				break;
			}
		}
		void mesh::set_size_array(int count) const
		{
			file.write(reinterpret_cast<char*>(&count), 4);
		}

		std::string mesh::get_name_section()
		{
			char buffer[512];
			int size = 0;
			for (char currentSymb = file.get(); currentSymb != '\0'; size++, currentSymb = file.get()) {
				if (size >= 512)
					throw TRACE_EXCEPTION("Out of range");
				buffer[size] = currentSymb;
			}
			buffer[size] = '\0';
			return string(buffer);
		}
		std::string mesh::get_name_element()
		{
			char buffer[256];
			auto size = get_bytes_size();
			if (size >= 256)
				throw TRACE_EXCEPTION("Value string >= 256 symbols");
			file.read(buffer, size);
			buffer[size] = '\0';
			return string(buffer);
		}
		std::string *mesh::get_value_strings(int count)
		{
			char buffer[1024];
			std::string *buffer_str = new std::string[count];
			for (int i = 0; i < count; ++i) {
				int size = get_int_sizes();
				if (size >= 1024)
					throw TRACE_EXCEPTION("Value string >= 1024 symbols");
				file.read(buffer, size);
				buffer_str[i] = std::string(buffer);
			}
			return buffer_str;
		}
		bool mesh::change_node_name(mesh_section &parent, const char *name, unsigned id)
		{
			if (parent.get_id() == id) {
				parent.set_name(name);
				return true;
			}

			for (auto &item_float : *parent.get_elements_float()) {
				if (item_float.second.get_id() == id) {
					item_float.second.SetName(name);
					return true;
				}
			}
			for (auto &item_int : *parent.get_elements_int()) {
				if(item_int.second.get_id() == id) {
					item_int.second.SetName(name);
					return true;
				}
			}
			for (auto &item_string : *parent.get_elements_string()) {
				if (item_string.second.get_id() == id) {
					item_string.second.SetName(name);
					return true;
				}
			}

			for (auto &section : *parent.get_sections()) {
				if (change_node_name(section, name, id))
					return true;
			}

			return false;
		}
		size_t mesh::get_bytes_size()
		{
			char object;
			file.read(reinterpret_cast<char	*>(&object), sizeof(char));
			return object;
		}
		size_t mesh::get_int_sizes()
		{
			int object;
			file.read(reinterpret_cast<char	*>(&object), sizeof(int));
			return object;
		}
		float *mesh::get_float_elements(int count)
		{
			float *arr = new float[count];
			file.read(reinterpret_cast<char*>(arr), sizeof(float) * count);
			return arr;
		}
		int *mesh::get_int_elements(int count)
		{
			int *arr = new int[count];
			file.read(reinterpret_cast<char*>(arr), sizeof(int) * count);
			return arr;
		}
		int mesh::get_level_section()
		{
			int count = 0;
			for (char currentSymb = file.get(); currentSymb == '['; count++, currentSymb = file.get());
			file.unget();
			return count;
		}
	}
}