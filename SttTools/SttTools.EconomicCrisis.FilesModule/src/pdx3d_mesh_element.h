#pragma once

namespace pdx_document
{
	namespace graph
	{
		template<class T>
		class mesh_element
		{
		public:
			mesh_element() = delete;
			mesh_element(const std::string &name, T *_value, const int count, const unsigned id);
			inline unsigned get_id() const { return id; }
			inline const std::string GetName() const { return name; }
			inline const T *GetValue() const { return value.get(); }
			inline int GetCount() const { return count; }
			void SetName(const std::string &name);
			void SetValue(T *value, const int count);
		private:
			unsigned id;
			std::string name;
			int count;
			std::unique_ptr<T[]> value;
		};

		template<class T>
		mesh_element<T>::mesh_element(const std::string & name, T *_value, const int count, const unsigned id) : count(count), id(id), name(name)
		{
			this->value.reset(_value);
		}

		template<class T>
		void mesh_element<T>::SetName(const std::string &name)
		{
			 this->name = name;
		}
		template<class T>
		void mesh_element<T>::SetValue(T *value, const int count)
		{
			this->value.reset(value);
			this->value = value;
			this->count = count;
		}
	}
}