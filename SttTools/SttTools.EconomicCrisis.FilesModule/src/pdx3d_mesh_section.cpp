#include "pdx3d_mesh_section.h"

namespace pdx_document
{
	namespace graph
	{
		void mesh_section::set_element(int *value, int count, std::string &name)
		{
			elements_int.insert(std::make_pair(name, mesh_element<int>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_element(float *value, int count, std::string &name)
		{
			elements_float.insert(std::make_pair(name, mesh_element<float>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_element(std::string *value, int count, std::string &name)
		{
			elements_char.insert(std::make_pair(name, mesh_element<std::string>(name, value, count, get_uniqueId())));
		}
		void mesh_section::set_section(const std::string &value, unsigned id)
		{
			sections.push_back(mesh_section(value, id));
		}

		json mesh_section::get_json(int max_return_values_str, int max_return_values_float, int max_return_values_int) const
		{
			json json_object;
			convert_to_json(json_object, max_return_values_str, max_return_values_float, max_return_values_int);
			return json_object;
		}
		void mesh_section::convert_to_json(nlohmann::basic_json<>& base, int max_return_values_str, int max_return_values_float, int max_return_values_int) const
		{
			base["name"] = name;
			base["id"] = id;
			base["element_string"] = nlohmann::json::object();
			base["element_int"] = nlohmann::json::object();
			base["element_float"] = nlohmann::json::object();
			base["sections"] = nlohmann::json::array();
			int index = 0;
			std::string old_key;
			for (auto it = elements_char.begin(); it != elements_char.end(); it = elements_char.upper_bound(it->first)) {
				base["element_string"].push_back({ it->second.GetName(), json::array() });
				for (auto iter = elements_char.equal_range(it->first); iter.first != iter.second; ++iter.first) {
					base["element_string"][it->second.GetName()].push_back({ { "id", iter.first->second.get_id() },{ "count", iter.first->second.GetCount() },{ "value",
						convert_to_vector<std::string>(iter.first->second.GetValue(), correct_count(max_return_values_str, iter.first->second.GetCount())) } });
				}
			}
			for (auto it = elements_int.begin(); it != elements_int.end(); it = elements_int.upper_bound(it->first)) {
				base["element_int"].push_back({ it->second.GetName(), json::array() });
				for (auto iter = elements_int.equal_range(it->first); iter.first != iter.second; ++iter.first) {
					base["element_int"][it->second.GetName()].push_back({ { "id", iter.first->second.get_id() },{ "count", iter.first->second.GetCount() },{ "value",
						convert_to_vector<int>(iter.first->second.GetValue(), correct_count(max_return_values_int, iter.first->second.GetCount())) } });
				}
			}
			for (auto it = elements_float.begin(); it != elements_float.end(); it = elements_float.upper_bound(it->first)) {
				base["element_float"].push_back({ it->second.GetName(), json::array() });
				for (auto iter = elements_float.equal_range(it->first); iter.first != iter.second; ++iter.first) {
					base["element_float"][it->second.GetName()].push_back({ { "id", iter.first->second.get_id() },{ "count", iter.first->second.GetCount() },{ "value",
						convert_to_vector<float>(iter.first->second.GetValue(), correct_count(max_return_values_float, iter.first->second.GetCount())) } });
				}
			}
			index = 0;
			for (auto &item : sections)
				item.convert_to_json(base["sections"][index++], max_return_values_str, max_return_values_float, max_return_values_int);
		}

		mesh_section &mesh_section::get_section(unsigned id)
		{
			for (auto &item : sections) {
				if (item.id == id)
					return item;
			}

			throw TRACE_EXCEPTION("ID not exists: " + std::to_string(id));
		}

		const int *mesh_section::get_values_int(unsigned id) const
		{
			return get_values_int(*this, id);
		}
		const float *mesh_section::get_values_float(unsigned id) const
		{
			return get_values_float(*this, id);
		}
		const int *mesh_section::get_values_int(const mesh_section &parent, unsigned id) const
		{
			auto result = get_value(&parent.elements_int, id);

			for (auto it = parent.sections.begin(); result == nullptr && it != parent.sections.end(); ++it)
				result = get_values_int(*it, id);

			return result;
					
		}
		const float *mesh_section::get_values_float(const mesh_section &parent, unsigned id) const
		{
			auto result = get_value(&parent.elements_float, id);

			for (auto it = parent.sections.begin(); result == nullptr && it != parent.sections.end(); ++it)
				result = get_values_float(*it, id);

			return result;
		}

		unsigned get_uniqueId()
		{
			static unsigned index = 1;
			return index++;
		}
		int correct_count(int count, int max)
		{
			if (count < 0)
				count = 0;
			if (count > max)
				count = max;

			return count;
		}
		template<class T>
		std::vector<T> convert_to_vector(const T *ptr, size_t count)
		{
			std::vector<T> arr(count);
			for (int i = 0; i < count; ++i)
				arr[i] = ptr[i];

			return arr;
		}
		template<class T>
		const T *get_value(const std::multimap<std::string, mesh_element<T>> *models, unsigned id)
		{
			for (auto it = models->begin(); it != models->end(); ++it) 
				for (auto iter = models->equal_range(it->first); iter.first != iter.second; ++iter.first)
					if (iter.first->second.get_id() == id) 
						return iter.first->second.GetValue();
			
			return nullptr;
		}
	}
}