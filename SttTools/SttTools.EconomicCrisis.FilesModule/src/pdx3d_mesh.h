#pragma once

#include "MeshResources.h"
#include "pdx3d_mesh_section.h"
#include "..\api_struct.h"
#include <memory>

namespace pdx_document
{
	namespace graph
	{
		namespace fs = boost::filesystem;
		using json = nlohmann::json;
		using std::string;
		using Resources::TypeMeshElement;
		using Resources::TypeMeshSection;

		class mesh
		{
		public:
			mesh() = delete;
			mesh(mesh &obj) = delete;
			mesh(const std::string &name);
			mesh(const char *name);
			mesh(const fs::path &path);
			inline std::string get_name_file() const { return full_path.filename().string(); }
			bool load();
			bool save(int mode);
			const char *get_json_string(int max_return_values_str = 1, int max_return_values_float = 0, int max_return_values_int = 0) const;
			const int *get_value_int(unsigned id) const;
			const float *get_value_float(unsigned id) const;
			bool change_node_name(const char *name, unsigned id);
		private:
			mesh_section data_structure;
			bool is_supported_version();
			fs::path full_path;
			mutable fs::fstream file;
			bool is_changed;
			mutable std::string json_result;
			
			const string general_id = "@@b@";

			void load(mesh_section &parent, int currentLevel = 0);
			void save(mesh_section &parent, int current_level = 0) const;
			void load_element(mesh_section &section);

			void insert_general_id();
			void insert_name_node(const std::string &name) const;

			std::string get_name_section();
			std::string get_name_element();
			std::string *get_value_strings(int count);
			bool change_node_name(mesh_section &parent, const char *name, unsigned id);

			TypeMeshSection get_type_section();
			TypeMeshElement get_type_element();
			void set_type_section(const TypeMeshSection type, int level = 0) const;
			void set_type_element(TypeMeshElement type) const;
			void set_size_array(int count) const;

			size_t get_bytes_size();
			size_t get_int_sizes();
			float* get_float_elements(int count);
			int* get_int_elements(int count);
			int get_level_section();
		};
	}
}