#include "stdafx.h"

#include <map> 
#include <string>
#include <iostream>
#include <vector>
//#include <strstream>      // std::stringstream
//#include <typeinfo>
//#include <iterator>
//#include <algorithm>
//
//#ifdef _DEBUG
//#include <crtdbg.h>
//#define _CRTDBG_MAP_ALLOC
////
//#include "src\MeshDocument.h"
#include "src\pdx3d_mesh.h"
#include <memory>
//
//
//

std::unique_ptr<pdx_document::graph::mesh> mesh_document;
std::unique_ptr<api_model::return_array<int>> array_int;
std::unique_ptr<api_model::return_array<float>> array_float;

const char *__stdcall load(const char *path, int max_values_str, int max_values_float, int max_values_int)
{
	mesh_document.reset(new pdx_document::graph::mesh(path));
	mesh_document->load();
	return mesh_document->get_json_string(max_values_str, max_values_float, max_values_int);
}

api_model::return_array<int>* __stdcall get_values_int(unsigned id, size_t count)
{
	// todo: problem with count and out of range if count upper than count array
	array_int.reset(new api_model::return_array<int>());
	array_int->pointer = const_cast<int*>(mesh_document->get_value_int(id));
	if (!array_int->pointer)
		array_int->count = 0;
	else
		array_int->count = count;

	return array_int.get();
}
void init()
{
	for (int i = 0; i < 1; ++i) {
		auto ch = load("D:\\Itvdn\\VEN_infantry.mesh", 1, 2, 3);
		auto rn1 = mesh_document->change_node_name("new_name", 3);
		auto rn12 = mesh_document->change_node_name("new_name2", 12);

		mesh_document->save(0);
	}
}

int main()
{
	/*_CrtMemState _ms;
	_CrtMemCheckpoint(&_ms)*/;
	system("pause");
	auto st = clock();
	init();
	std::cout << clock() - st << std::endl;
	system("pause");
	//_CrtMemDumpAllObjectsSince(&_ms);
	return 0;
}