#pragma once

namespace api_model
{
	template<class T>
	struct return_array
	{
		int count;
		T *pointer;
	};
}