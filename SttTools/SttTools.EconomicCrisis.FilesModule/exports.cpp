#include "src\pdx3d_mesh.h"
#include "api_struct.h"
#include <memory>

extern "C"
{
	std::unique_ptr<pdx_document::graph::mesh> mesh_document;
	std::unique_ptr<api_model::return_array<int>> array_int;
	std::unique_ptr<api_model::return_array<float>> array_float;

	const char* __stdcall load(const char *path, int max_values_str, int max_values_float, int max_values_int)
	{
		mesh_document.reset(new pdx_document::graph::mesh(path));
		mesh_document->load();
		return mesh_document->get_json_string(max_values_str, max_values_float, max_values_int);
	}

	const char* __stdcall reload_json()
	{
		return mesh_document->get_json_string(1, 2, 2);
	}

	api_model::return_array<int>* __stdcall get_values_int(unsigned id, size_t count)
	{
		// todo: problem with count and out of range if count upper than count array
		array_int.reset(new api_model::return_array<int>());
		array_int->pointer = const_cast<int*>(mesh_document->get_value_int(id));
		if (!array_int->pointer)
			array_int->count = 0;
		else
			array_int->count = count;

		return array_int.get();
	}

	api_model::return_array<float>* __stdcall get_values_float(unsigned id, size_t count)
	{
		// todo: problem with count and out of range if count upper than count array
		array_float.reset(new api_model::return_array<float>());
		array_float->pointer = const_cast<float*>(mesh_document->get_value_float(id));
		if (!array_float->pointer)
			array_float->count = 0;
		else
			array_float->count = count;

		return array_float.get();
	}

	bool __stdcall change_node_name(const char *new_name, unsigned id)
	{
		return mesh_document->change_node_name(new_name, id);
	}
	
	bool __stdcall save(int mode)
	{
		return mesh_document->save(mode);
	}
}