﻿using System;
using System.Runtime.InteropServices;
using System.Text;

namespace SttTools.APIModule
{
    public unsafe static class ECModule
    {
        [DllImport("ECModule.dll", EntryPoint = "load", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr LoadMesh(StringBuilder str, int maxValuesStr, int maxValuesFloat, int maxValuesInt);

        [DllImport("ECModule.dll", EntryPoint = "reload_json", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr ReloadData();

        [DllImport("ECModule.dll", EntryPoint = "get_values_int", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetValuesInt([MarshalAs(UnmanagedType.U4)]uint id, [MarshalAs(UnmanagedType.U4)]uint count);

        [DllImport("ECModule.dll", EntryPoint = "get_values_float", CallingConvention = CallingConvention.StdCall)]
        public static extern IntPtr GetValuesFloat([MarshalAs(UnmanagedType.U4)]uint id, [MarshalAs(UnmanagedType.U4)]uint count);

        [DllImport("ECModule.dll", EntryPoint = "change_node_name", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool ChangeNodeName(StringBuilder nodeName, [MarshalAs(UnmanagedType.U4)]uint id);

        [DllImport("ECModule.dll", EntryPoint = "save", CallingConvention = CallingConvention.StdCall)]
        [return: MarshalAs(UnmanagedType.Bool)]
        public static extern bool Save(int mode);
    }
}
