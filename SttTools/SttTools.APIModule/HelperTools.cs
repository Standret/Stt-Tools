﻿using SttTools.Domain.Entities.ResponseModel;
using System;
using System.Diagnostics;
using System.Runtime.InteropServices;

namespace SttTools.APIModule
{
    public static class HelperTools
    {
        public static string ConvertToString(IntPtr ptr)
        {
            return Marshal.PtrToStringAnsi(ptr);
        }
        public static T ConvertToStruct<T>(IntPtr ptr)
        {
            return Marshal.PtrToStructure<T>(ptr);
        }
        public static int[] ConvertToArrayInt(IntPtr ptr)
        {
            try
            {
                var array = ConvertToStruct<ReturnArray>(ptr);
                if (array.count == 0)
                    return null;

                int[] arr = new int[array.count];
                Marshal.Copy(array.pointer, arr, 0, (int)array.count);

                return arr;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
        public static float[] ConvertToArrayFloat(IntPtr ptr)
        {
            try
            {
                var array = ConvertToStruct<ReturnArray>(ptr);
                if (array.count == 0)
                    return null;

                float[] arr = new float[array.count];
                Marshal.Copy(array.pointer, arr, 0, (int)array.count);

                return arr;
            }
            catch (Exception ex)
            {
                Debug.WriteLine(ex.Message);
                return null;
            }
        }
    }
}
