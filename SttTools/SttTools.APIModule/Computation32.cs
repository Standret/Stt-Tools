﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.APIModule
{
    unsafe public class Computation32
    {
        [DllImport("Computation32.dll", EntryPoint = "json_c", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr Json();

        [DllImport("Computation32.dll", EntryPoint = "convert_notation", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern IntPtr ConvertNotation(StringBuilder str, int from, int to);

        [DllImport("Computation32.dll", EntryPoint = "calculate_expression", CharSet = CharSet.Ansi, CallingConvention = CallingConvention.Cdecl)]
        public static extern double CalculateExpression(StringBuilder str);

        public static string ConvertToString(IntPtr ptr)
        {
            return Marshal.PtrToStringAnsi(ptr);
        }
    }
}
