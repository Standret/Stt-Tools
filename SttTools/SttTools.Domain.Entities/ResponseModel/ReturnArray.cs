﻿using System;
using System.Runtime.InteropServices;

namespace SttTools.Domain.Entities.ResponseModel
{
    [StructLayout(LayoutKind.Sequential)]
    public struct ReturnArray
    {
        [MarshalAs(UnmanagedType.U4)]
        public uint count;
        public IntPtr pointer;
    }
}
