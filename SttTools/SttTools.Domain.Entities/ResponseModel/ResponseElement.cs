﻿using Newtonsoft.Json;

namespace SttTools.Domain.Entities.ResponseModel
{
    public class ResponseElement<T>
    {
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("value")]
        public T[] Value { get; set; }
        [JsonProperty("count")]
        public int Count { get; set; }
    }
}
