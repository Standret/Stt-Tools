﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.ResponseModel
{
    public class MeshStructure
    {
        [JsonProperty("name")]
        public string Name { get; set; }
        [JsonProperty("id")]
        public int Id { get; set; }
        [JsonProperty("element_float")]
        public Dictionary<string, ResponseElement<float>[]> ElementsFloat { get; set; }
        [JsonProperty("element_int")]
        public Dictionary<string, ResponseElement<int>[]> ElementsInt { get; set; }
        [JsonProperty("element_string")]
        public Dictionary<string, ResponseElement<string>[]> ElementsString { get; set; }
        [JsonProperty("sections")]
        public MeshStructure[] Sections { get; set; }
    }
}
