﻿using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using System.Collections.Generic;

namespace SttTools.Domain.Entities.ViewModels.CreateProject
{
    public class TemplatesCreateObjectsViewModel
    {
        public List<TemplatesCreateObjectsViewModel> Children = new List<TemplatesCreateObjectsViewModel>();
        public IEnumerable<TemplatesCreateObjectsViewModel> ChildrenNode { get { return Children; } }

        public List<TemplateObjects> Projects { get; set; }
        public string Name { get; set; }

        public TemplatesCreateObjectsViewModel(string name, List<TemplateObjects> projects = null)
        {
            Name = name;
            Projects = projects;
        }
    }
}
