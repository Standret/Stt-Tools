﻿namespace SttTools.Domain.Entities.ViewModels.CreateProject
{
    public class TemplateObjects
    {
        public string Name { get; set; }
        public string TypeObjects { get; set; }
        public string Description { get; set; }
        public string Image { get; set; }
        public int Id { get; set; }

        public TemplateObjects() { }
        public TemplateObjects(string name, string typeObjects, string description, string image, int id)
        {
            Name = name;
            TypeObjects = typeObjects;
            Description = description;
            Image = image;
            Id = id;
        }
    }
}
