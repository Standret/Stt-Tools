﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum.SettingsPrograms
{
    public enum FileType : byte
    {
        Asset,
        Mesh,
        Anim
    }
}
