﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum
{
    public enum TypeModalWindow : byte
    {
        Converter, Calculator
    }
}
