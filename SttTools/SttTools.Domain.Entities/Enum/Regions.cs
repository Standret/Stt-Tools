﻿namespace SttTools.Domain.Entities.Enum
{
    public enum Regions : byte
    {
        Shell,
        Menu,
        Main,
        ExplorerMain,
        ExplorerSecondary
    }
}
