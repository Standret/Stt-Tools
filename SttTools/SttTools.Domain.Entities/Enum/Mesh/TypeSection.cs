﻿namespace SttTools.Domain.Entities.Enum
{
    public enum TypeSection : byte
    {
        Section,
        Int,
        Float,
        String
    }
}
