﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum
{
    public enum CalculatorVariable : byte
    {
        Zero,
        One,
        Two,
        Three,
        Four,
        Five,
        Six,
        Seven,
        Eight,
        Nine,
        ChangeSign,
        Point,
        Result,
        Plus,
        Minus,
        Multiple,
        Divide,
        Backspace,
        C,
        CE,
        Sin,
        Cos,
        Sqr,
        Sqrt
    }
}
