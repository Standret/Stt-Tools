﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum
{
    public enum Notation : byte
    {
        Binary, Decimal, Hexadecimal
    }
}
