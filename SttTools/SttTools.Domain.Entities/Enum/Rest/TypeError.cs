﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum.Rest
{
    public enum TypeError : byte
    {
        Exception,
        JsonIsNullOrEmpty,
        ErrorDeserialize,
        Default,
        Timeout,
        Unkown
    }
}
