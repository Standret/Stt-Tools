﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum.Rest
{
    public enum ContentType : byte
    {
        Json,
        UrlEncoded,
        PngImage
    }
}
