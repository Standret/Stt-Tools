﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Enum.CustomPopups
{
    public enum Buttons
    {
        Unkown,
        Yes = 1,
        No = 2,
        Cancel = 4
    }
}
