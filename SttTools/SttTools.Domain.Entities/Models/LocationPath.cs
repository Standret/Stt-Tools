﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models
{
    public class LocationPath
    {
        public string BasePath { get; set; } 
        public string NameFile { get; set; } = "Project";
        public string ExtensionFile { get; set; } = "unkown";
        public bool UniqueProjectFolder { get; set; }

        public override string ToString()
        {
            return String.Format("{0}\\{1}{2}.{3}", BasePath, UniqueProjectFolder ? NameFile + "\\" : "", NameFile, ExtensionFile);
        }

        public string GetFolderPath()
        {
            return String.Format("{0}{1}", BasePath, UniqueProjectFolder ? "\\" + NameFile : "");
        }
        public string GetFilePath()
        {
            return ToString();
        }
    }
}
