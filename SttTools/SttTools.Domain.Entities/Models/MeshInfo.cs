﻿namespace SttTools.Domain.Entities.Models
{
    public class MeshInfo
    {
        public string Diffuse { get; set; }
        public string Normal { get; set; }
        public string Specular { get; set; }
        public string Shader { get; set; }

        public MeshInfo(string diffuse, string normal, string specular, string shader)
        {
            Diffuse = diffuse;
            Normal = normal;
            Specular = specular;
            Shader = shader;
        }
    }
}
