﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models
{
    public class WindowData
    {
        public string Title { get; set; }
        public int Height { get; set; }
        public int Width { get; set; }
    }
}
