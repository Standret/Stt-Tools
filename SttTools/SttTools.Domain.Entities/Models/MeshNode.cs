﻿using SttTools.Domain.Entities.Enum;
using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SttTools.Domain.Entities.Models
{
    public class MeshNode
    {
        public ObservableCollection<MeshNode> ChildrenGeneral { get; set; } = new ObservableCollection<MeshNode>();
        public IEnumerable<MeshNode> Children { get { return ChildrenGeneral; } }

        public TypeSection TypeSection { get; private set; }
        public string TypeSectionString { get { return TypeSection.ToString(); } }
        public string Name { get; set; }
        public string Value { get; private set; }
        public int Count { get; private set; }
        public int Id { get; private set; }

        public MeshNode(string name, TypeSection type, int id = -1, int count = 0, string value = "")
        {
            Name = name;
            TypeSection = type;
            Count = count;
            Value = value ?? string.Empty;
            Id = id;
        }
    }
}
