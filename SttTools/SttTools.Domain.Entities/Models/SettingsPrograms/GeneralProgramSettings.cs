﻿using SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels;
using System.Collections.Generic;

namespace SttTools.Domain.Entities.Models.SettingsPrograms
{
    public class GeneralProgramSettings
    {
        public List<ProjectSettings> Projects { get; set; }
        public BaseTemplate Templates { get; set; }
    }
}
