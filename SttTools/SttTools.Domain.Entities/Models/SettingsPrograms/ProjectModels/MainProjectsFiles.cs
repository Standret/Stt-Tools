﻿using System.Collections.Generic;

namespace SttTools.Domain.Entities.Models.SettingsPrograms.ProjectModels
{
    public class MainProjectsFiles
    {
        public string BasePath { get; set; }
        public List<string> Files { get; set; }
        public List<MainProjectsFiles> Folders { get; set; }
    }
}
