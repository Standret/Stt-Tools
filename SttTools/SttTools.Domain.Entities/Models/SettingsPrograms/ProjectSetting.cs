﻿using Newtonsoft.Json;
using SttTools.Domain.Entities.Enum.SettingsPrograms;
using System;

namespace SttTools.Domain.Entities.Models
{
    public class ProjectSettings
    {
        public string FullPath { get; set; }
        public DateTimeOffset LastOpen { get; set; }
        public bool Picked { get; set; }
        public TypeIconProjects TypeIcon { get; set; }

        [JsonIgnore]
        public string ProjectName
        {
            get
            {
                int index = FullPath.LastIndexOf('\\');
                int indexPoint = FullPath.LastIndexOf('.');
                if (index == -1 || indexPoint == -1)
                    return "Error components: " + FullPath ?? "null";
                return FullPath.Substring(index + 1, indexPoint - index - 1);
            }
        }

        public ProjectSettings(string fullPath)
        {
            FullPath = fullPath;
            LastOpen = DateTimeOffset.Now;
            TypeIcon = TypeIconProjects.Defolts;
        }
    }
}
