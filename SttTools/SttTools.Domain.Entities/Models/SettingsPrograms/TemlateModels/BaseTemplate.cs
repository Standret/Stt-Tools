﻿using System.Collections.Generic;

namespace SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels
{
    public class BaseTemplate
    {
        public Dictionary<string, List<TemplateProject>> TemplateProjects { get; set; }
    }
}
