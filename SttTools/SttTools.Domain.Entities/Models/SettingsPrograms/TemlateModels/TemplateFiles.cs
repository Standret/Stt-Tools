﻿using Newtonsoft.Json;
using SttTools.Domain.Entities.Enum.SettingsPrograms;

namespace SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels
{
    public class TemplateFiles
    {
        public FileType FileType { get; set; }

        public string Name { get; set; }
        public string Image { get; set; }
        public string Decription { get; set; }

        [JsonIgnore]
        public string DefaultProjectName { get { return FileType.ToString(); } }
        [JsonIgnore]
        public int Id { get; set; }

        public string ExtensionsProjectFile { get; set; }
    }
}
