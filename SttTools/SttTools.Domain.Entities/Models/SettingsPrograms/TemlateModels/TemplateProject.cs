﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace SttTools.Domain.Entities.Models.SettingsPrograms.TemlateModels
{
    public class TemplateProject
    { 
        public string Name { get; set; }
        public string Decription { get; set; }
        public string Image { get; set; }

        [JsonIgnore]
        public string TypeProject { get; set; }
        [JsonIgnore]
        public int Id { get; set; }

        public string DefaultProjectName { get; set; }
        public string ExtensionsProjectFile { get; set; }

        public bool CreateBaseArchitecture { get; set; }
        public bool CreateDirectoryForProject { get; set; }

        public Dictionary<string, List<TemplateFiles>> TemplateFiles { get; set; }
    }
}
