﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models
{
    public class BaseSolutionSettings
    {
        public ObservableCollection<NodeCatalog> Catalogs { get; set; }
    }
}
