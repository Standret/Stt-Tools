﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models.SpecialModel
{
    public class PriorityQueue<TPriority, TItem>
    {
        private readonly SortedDictionary<TPriority, Queue<TItem>> _subQueues;

        public PriorityQueue(IComparer<TPriority> priorityComparer)
        {
            _subQueues = new SortedDictionary<TPriority, Queue<TItem>>(priorityComparer);
        }
        public PriorityQueue() : this(Comparer<TPriority>.Default) { }

        public bool HasItems { get { return _subQueues.Any(); } }
        public int Count { get { return _subQueues.Sum(q => q.Value.Count); } }

        private void AddQueueOfPriority(TPriority priority)
        {
            _subQueues.Add(priority, new Queue<TItem>());
        }

        public void Enqueue(TPriority priority, TItem item)
        {
            lock (_subQueues)
            {
                if (!_subQueues.ContainsKey(priority))
                    AddQueueOfPriority(priority);

                _subQueues[priority].Enqueue(item);
            }
        }
        public TItem Dequeue()
        {
            TItem nextItem = default(TItem);
            lock (_subQueues)
            {
                KeyValuePair<TPriority, Queue<TItem>> first = _subQueues.First();
                nextItem = first.Value.Dequeue();
                if (!first.Value.Any())
                {
                    _subQueues.Remove(first.Key);
                }
            }
            return nextItem;
        }
    }
}
