﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models.Rest
{
    public class ApiResultModel<TResult>
    {
        public bool IsSuccess { get; set; }
        public TResult SuccessResult { get; set; }
        public ErrorModel Error { get; set; }

        public ApiResultModel(bool isSucces, TResult succesResult = default(TResult), ErrorModel errorResult = null)
        {
            IsSuccess = isSucces;
            SuccessResult = succesResult;
            Error = errorResult;
        }
    }
}
