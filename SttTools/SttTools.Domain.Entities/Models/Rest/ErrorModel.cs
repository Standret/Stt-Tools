﻿using SttTools.Domain.Entities.Enum.Rest;
using System.Net;

namespace SttTools.Domain.Entities.Models.Rest
{
    public class ErrorModel
    {
        public TypeError Type { get; set; }
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessage { get; set; }

        public ErrorModel(TypeError type, HttpStatusCode statusCode = HttpStatusCode.OK, string errorMessage = null)
        {
            Type = type;
            StatusCode = statusCode;
            ErrorMessage = errorMessage;
        }
    }
}
