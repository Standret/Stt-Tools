﻿namespace SttTools.Domain.Entities.Models.Rest
{
    public class UriData
    {
        public string BaseUri { get; set; }
        public string TypeToken { get; set; }
        public string AccesToken { get; set; }
        public bool IsSynchronized { get; set; }
    }
}
