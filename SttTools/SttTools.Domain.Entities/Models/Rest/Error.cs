﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models.Rest
{
    public class Error
    {
        public HttpStatusCode Code { get; set; }
        public string Description { get; set; }
    }
}
