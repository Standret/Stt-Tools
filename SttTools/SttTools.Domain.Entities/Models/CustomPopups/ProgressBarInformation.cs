﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models.CustomPopups
{
    public class ProgressBarInformation : BaseViewableModel
    {
        private List<string> ListDescriptions;
        public string Description { get; set; }

        private int current;
        public int Current
        {
            get => current;
            set
            {
                if (value >= Total)
                    value = Total;
                if (value < 0)
                    value = 0;

                current = value;
                Description = ListDescriptions[value];
                RaisePropertyChanged(() => Description);
                RaisePropertyChanged(() => Current);
            }
        }

        public int Total { get; set; }

        public ProgressBarInformation(List<string> descriptions)
        {
            ListDescriptions = descriptions;
            Current = 0;
            Total = descriptions.Count;
        }
    }
}
