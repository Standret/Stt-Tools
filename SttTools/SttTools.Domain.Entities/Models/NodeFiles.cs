﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace SttTools.Domain.Entities.Models
{
    public class NodeCatalog
    {
        public ObservableCollection<NodeCatalog> Children = new ObservableCollection<NodeCatalog>();
        public IEnumerable<NodeCatalog> ChildrenNode { get { return Children; } }
        public string FullPath { get; set; }
        public string Name
        {
            get
            {
                var index = FullPath?.LastIndexOf('\\');
                if (index.HasValue && index.Value == -1)
                    return "Error: " + FullPath ?? "null";

                return FullPath.Substring(index.Value + 1);
            }
        }
        public string Extension
        {
            get
            {
                var index = FullPath?.LastIndexOf('.');
                if (index.HasValue && index.Value == -1)
                    return "Error: " + FullPath ?? "null";

                return FullPath.Substring(index.Value + 1);
            }
        }

        public NodeCatalog(string fullPath)
        {
            FullPath = fullPath;
        }
    }
}
