﻿using SttTools.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SttTools.Domain.Entities.Models
{
    public class ProjectDataModel
    {
        public string Name { get; set; }
        public string Description { get; set; }
    }
}
