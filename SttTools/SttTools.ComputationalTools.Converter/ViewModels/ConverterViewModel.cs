﻿using Prism.Mvvm;
using SttTools.Domain.Entities.Enum;
using SttTools.APIModule;
using System;
using System.Text;
using System.Text.RegularExpressions;

namespace SttTools.ComputationalTools.Converter.ViewModels
{
    public class ConverterViewModel : BindableBase
    {
        public ConverterViewModel()
        {
            dec = hex = bin = String.Empty;
        }

        private string dec;
        public string Dec
        {
            get { return dec; }
            set
            {
                dec = value;
                CalculateVariable(Notation.Decimal);
                RaisePropertyChanged("Dec");
            }
        }
        private string hex;
        public string Hex
        {
            get { return hex; }
            set
            {
                hex = value;
                CalculateVariable(Notation.Hexadecimal);
                RaisePropertyChanged("Hex");
            }
        }
        private string bin;
        public string Bin
        {
            get { return bin; }
            set
            {
                bin = value;
                CalculateVariable(Notation.Binary);
                RaisePropertyChanged("Bin");
            }
        }

        private bool isError;
        public bool IsError
        {
            get { return isError; }
            set
            {
                isError = value;
                RaisePropertyChanged("IsError");
            }
        }

        private bool _lockCalculate = false;
        private void CalculateVariable(Notation notation)
        {
            if (_lockCalculate)
                return;

            _lockCalculate = true;

            switch (notation)
            {
                case Notation.Binary:
                    if (Regex.IsMatch(bin, binaryPattern))
                    {
                        IsError = false;
                        Dec = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(bin), 2, 10));
                        Hex = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(bin), 2, 16));
                    }
                    else if (!String.IsNullOrWhiteSpace(bin))
                    {
                        Dec = Hex = 0.ToString();
                        IsError = true;
                    }
                    break;
                case Notation.Decimal:
                    if (Regex.IsMatch(dec, decimalPattern))
                    {
                        IsError = false;
                        Bin = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(dec), 10, 2));
                        Hex = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(dec), 10, 16));
                    }
                    else if (!String.IsNullOrWhiteSpace(dec))
                    {
                        Bin = Hex = 0.ToString();
                        IsError = true;
                    }
                    break;
                case Notation.Hexadecimal:
                    if (Regex.IsMatch(hex, hexadeciamalPattern))
                    {
                        IsError = false;
                        Bin = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(hex), 16, 2));
                        Dec = Computation32.ConvertToString(Computation32.ConvertNotation(new StringBuilder(hex), 16, 10));
                    }
                    else if (!String.IsNullOrWhiteSpace(hex))
                    {
                        Bin = Dec = 0.ToString();
                        IsError = true;
                    }
                    break;
            }
            _lockCalculate = false;
        }

        private const string binaryPattern = @"^[0-1]+\.?[0-1]*$";
        private const string decimalPattern = @"^\d+\.?\d*$";
        private const string hexadeciamalPattern = @"^[\da-fA-F]+\.?[\da-fA-F]*$";
        private const string errorMessage = "Incorect input";
    }
}