﻿using Prism.Commands;
using Prism.Mvvm;
using SttTools.Domain.Entities.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace SttTools.ComputationalTools.Converter.ViewModels
{
    class CalculatorViewModel : BindableBase
    {
       public DelegateCommand<CalculatorVariable?> Calculate { get; private set; }

        public CalculatorViewModel()
        {
            Calculate = new DelegateCommand<CalculatorVariable?>(OnCalculate);
            Editor = Result = "0";
        }

        private void OnCalculate(CalculatorVariable? var)
        {
            if (var == null)
                return;

            if (var <= CalculatorVariable.Nine && var >= CalculatorVariable.Zero)
            {
                if (Editor[Editor.Length - 1] == '0')
                    Editor = Editor.Remove(Editor.Length - 1);
                Editor += (int)var;
            }

            if (var == CalculatorVariable.Sin || var == CalculatorVariable.Cos ||
                var == CalculatorVariable.Sqr || var == CalculatorVariable.Sqrt)
            {
                AddFunction(var.Value);
            }

            switch (var)
            {
                case CalculatorVariable.Plus:
                    Editor += '+';
                    break;
                case CalculatorVariable.Minus:
                    Editor += '-';
                    break;
                case CalculatorVariable.Divide:
                    Editor += '/';
                    break;
                case CalculatorVariable.Multiple:
                    Editor += '*';
                    break;
                case CalculatorVariable.Point:
                    Editor += '.';
                    break;
                case CalculatorVariable.Backspace:
                    if (Editor.Length == 0)
                        Editor = Result = "0";
                    else
                        Editor = Editor.Remove(Editor.Length - 1);
                    break;
                case CalculatorVariable.C:
                    Result = Editor = "0";
                    break;
                case CalculatorVariable.CE:
                    Result = Editor = "0";
                    break;
                default:
                    break;
            }

            if (Editor.Length != 0 && (Editor[Editor.Length - 1] == ')' || Char.IsDigit(Editor[Editor.Length - 1])))
                Result = APIModule.Computation32.CalculateExpression(new StringBuilder(Editor)).ToString();
        }

        private void AddFunction(CalculatorVariable name)
        {
            for (int i = Editor.Length - 1; i >= 0; --i)
            {
                if (IsArrifmetive(Editor[i]) || i == 0)
                {
                    if (i == Editor.Length)
                        return;

                    Editor = Editor.Insert(i != 0 ? i + 1 : 0, name.ToString().ToLower() + "(");
                    Editor += ")";
                    return;
                }
            }
        }


        private bool IsArrifmetive(char symb)
        {
            return symb == '*' || symb == '/' || symb == '+' || symb == '-';
        }

        private string editor;
        public string Editor
        {
            get { return editor; }
            set
            {
                editor = value;
                RaisePropertyChanged("Editor");
            }
        }

        private string result;
        public string Result
        {
            get { return result; }
            set
            {
                result = value;
                RaisePropertyChanged("Result");
            }
        }

    }
}
