﻿using Microsoft.Practices.Unity;
using Prism.Modularity;
using Prism.Regions;
using Prism.Unity;
using SttTools.ComputationalTools.Converter.ViewModels;
using SttTools.ComputationalTools.Converter.Views;

namespace SttTools.ComputationalTools.Converter
{
    public class InitializeConverterModule : IModule
    {
        private IRegionManager _regionManager;
        private IUnityContainer _unityContainer;

        public InitializeConverterModule(IRegionManager regionManager, IUnityContainer unityContainer)
        {
            _regionManager = regionManager;
            _unityContainer = unityContainer;
        }

        public void Initialize()
        {
            _unityContainer.RegisterTypeForNavigation<ConverterView>("ConverterView");
            _unityContainer.RegisterTypeForNavigation<CalculatorView>("CalculatorView");
            _regionManager.RequestNavigate("ModuleShell", REQUEST_NAVIGATE_TO);
        }

        public static string REQUEST_NAVIGATE_TO { get; set; } // TODO: delete
    }
}
