﻿using Prism.Commands;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SttTools.ComputationalTools.Converter.CustomComponents
{
    /// <summary>
    /// Interaction logic for SignLabel.xaml5
    /// </summary>
    public partial class SignLabel : UserControl
    {
        public SignLabel()
        {
            InitializeComponent();
            _tfMain.GotFocus += (o, e) => _tfMain.Text = "";
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }
        public object Identifier
        {
            get { return (object)GetValue(IdentifierProperty); }
            set { SetValue(IdentifierProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(SignLabel), new FrameworkPropertyMetadata(OnChangeTextBox)
            {
                DefaultValue = "",
                BindsTwoWayByDefault = true,
                DefaultUpdateSourceTrigger = UpdateSourceTrigger.PropertyChanged
            });

        public static readonly DependencyProperty IdentifierProperty =
            DependencyProperty.Register("Identifier", typeof(object), typeof(SignLabel), new PropertyMetadata(OnChangeNameLabel) { DefaultValue = "UNK" });


        private static void OnChangeNameLabel(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
        {
            ((SignLabel)depObj)._lbIdentifier.Content = args.NewValue;
        }
        private static void OnChangeTextBox(DependencyObject depObj, DependencyPropertyChangedEventArgs args)
        {
            ((SignLabel)depObj)._tfMain.Text = (string)args.NewValue;
        }


        public GridLength WidthLabel { get; set; }
    }
}
